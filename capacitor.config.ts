import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'ionic.upmeup.app',
  appName: 'Up me Up',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
