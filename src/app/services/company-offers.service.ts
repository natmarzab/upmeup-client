/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { Apollo, gql, QueryRef } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { map } from 'rxjs/operators';
import { RequiredExperience } from '../models/required-experience.model';
import { OfferStatus } from '../models/offer-status.model';
import { Observable } from 'rxjs';

const GET_ALLOFFERS = gql`
query getOffers($filter: OfferFilter, $limit: Float, $skip: Float){
  getCompanyOffers(filter: $filter, limit: $limit, skip: $skip) {
    _id
    userId
    title
    eduLevel
    city
    workHours
    competencies {
      _id
      name
    }
    salaryRange
    remote
    enrolled
    contractType
    description
    requirements
    requiredExperience
    match
    createdDate
    candidates {
      status
      user {
        _id
      }
    }
    status
    user {
      _id
      name
      avatarB64
      softSkills {
        _id
        name
      }
      sector {
        name
      }
    }
    file
  }
}
`;

const GET_OFFER = gql`
query getDetailOffer($id: String!) {
  getOffer(id: $id) {
    _id
    userId
    title
    eduLevel
    city
    workHours
    competencies {
      _id
      name
    }
    salaryRange
    remote
    enrolled
    contractType
    description
    requiredExperience
    requirements
    createdDate
    status
    user {
      _id
      name
      avatarB64
      softSkills {
        _id
        name
      }
      sector {
        name
      }
      website
      rooms {
        userId
        roomId
      }
    }
    match
    candidates {
      user {
        _id
        avatarB64
        name
        surname
        email
        city
        jobPosition
        softSkills {
          _id
          name
        }
      }
      status
      match {
        total 
      }
    }
    file
  }
}
`;


const GET_CANDIDATURES = gql`
  query {
    myCandidatures {
      _id
      userId
      title
      eduLevel
      city
      workHours
      competencies {
        _id
        name
      }
      salaryRange
      remote
      enrolled
      contractType
      requiredExperience
      description
      requirements
      createdDate
      user {
        _id
        name
        avatarB64
        softSkills {
          _id
          name
        }
        sector {
          name
        }
      }
    }
  }
`

const MUT_CREATEOFFER = gql`
  mutation createOfferMut($userId: String!, 
                          $title: String!, 
                          $eduLevel: String!,
                          $competencies: [EmbeddedCompetency!]!,
                          $requiredExperience: RequiredExperience!,
                          $city: String!,
                          $workHours: String!, 
                          $salaryRange: String!, 
                          $remote: String!,
                          $enrolled: Float!, 
                          $contractType: String!,
                          $description: String!,
                          $requirements: String!,
                          $createdDate: DateTime!,
                          $status: OfferStatus,
                          $file: [String!]!
                          ) {
    createOffer(
      createOfferDto: { 
        userId: $userId
        title: $title
        eduLevel: $eduLevel
        competencies: $competencies
        city: $city
        workHours: $workHours
        salaryRange: $salaryRange
        requiredExperience: $requiredExperience
        remote: $remote
        enrolled: $enrolled
        contractType: $contractType
        description: $description
        requirements: $requirements
        createdDate: $createdDate
        user: $userId
        status: $status
        file: $file
      }    
    ) {
      _id
      userId
      title
      eduLevel
      competencies {
        _id
        name
      }
      city
      workHours
      salaryRange
      remote
      enrolled
      contractType
      description
      requirements
      createdDate
      status
      file
    }
  }
`;

const MUT_EDITOFFER = gql`
  mutation editOfferMut(  $id: String!, 
                          $title: String!, 
                          $eduLevel: String!,
                          $city: String!, 
                          $workHours: String!, 
                          $competencies: [EmbeddedCompetency!]!,
                          $salaryRange: String!,
                          $requiredExperience: RequiredExperience!,
                          $remote: String!,
                          $contractType: String!
                          $description: String!
                          $requirements: String!
                          $file: [String!]!
                        ) {
    updateOffer( 
      id: $id
      offerInputs: { 
            title: $title
            eduLevel: $eduLevel
            city: $city
            workHours: $workHours
            competencies: $competencies
            salaryRange: $salaryRange
            requiredExperience: $requiredExperience
            remote: $remote
            contractType: $contractType
            description: $description
            requirements: $requirements
            file: $file
      }    
    ) {
      _id
      userId
      title
      eduLevel
      city
      workHours
      competencies {
        _id
        name
      }
      salaryRange
      remote
      enrolled
      contractType
      description
      requirements
      createdDate
      status
      file
    }
  }
`;

const MUT_DELETEOFFER = gql`
  mutation deleteOfferMut($id: String!) {
    deleteOffer(id: $id) {
      _id
      title
    }
  }`;

const MUT_UPDOFFER_ADD_CANDIDATE = gql`
  mutation submitAddCandidate (
    $offerId: String!,
    $candidateId: String!
  ) {
    addCandidate(
      offerId: $offerId,
      candidateId: $candidateId
    ) {
      title
    }
  }
`;

const MUT_UPDOFFER_REMOVE_CANDIDATE = gql`
mutation submitRemoveCandidate (
  $offerId: String!,
  $candidateId: String!
) {
  removeCandidate(
    offerId: $offerId,
    candidateId: $candidateId
  ) {
    title
  }
}
`;

  const MUT_UPDOFFER_UPDATE_CANDIDATE = gql`
    mutation submitUpdateCandidate (
      $offerId: String!,
      $candidateId: String!,
      $status: CandidatureStatus!
    ) {
      updateCandidateStatus(
        offerId: $offerId,
        candidateId: $candidateId,
        status: $status
      ) {
        title
      }
    }
  `;

const MUT_UPDOFFER_ENROLL = gql`
  mutation submitUpdateEnroll(
                                $id: String!, 
                                $updateValue: UpdateOfferInput!
                              ) {
    updateOffer(
      id: $id, 
      offerInputs: $updateValue
    ) {
      title
      city
      enrolled
    }
  }
`;

const MUT_UPDATE_OFFER_STATUS = gql`
    mutation submitUpdateOffer (
      $offerId: String!,
      $status: OfferStatus!
    ) {
      updateOfferStatus(
        offerId: $offerId,
        status: $status
      ) {
        title
      }
    }
  `;

  const GET_UPLOAD_FILE_URL = gql`
  query getUploadOfferUrl($id: String!,
                          $filename: String!) {
    uploadOfferUrl(id: $id, 
                  filename: $filename) {
      presignedUrl
    }
  }
`

const GET_DOWNLOAD_FILE = gql`
  query getDownloadFile($id: String!,
                        $file: String!) {
    downloadFile(id: $id,
                 file: $file) {
      presignedUrl
    }
  }
`

const REFRESH = 10000;

@Injectable({
  providedIn: 'root'
})
export class CompanyOffersService {

  constructor(private apollo: Apollo) { }

  /**
   * Get All Offers from DB.
   */
  private _offersWatchQuery: QueryRef<any, EmptyObject>;

  qGetAllOffers(filter?: any, limit?: number, skip?: number): QueryRef<any, EmptyObject> {
    this._offersWatchQuery = this.apollo.watchQuery({
      query: GET_ALLOFFERS,
      variables: {
        filter,
        limit,
        skip
      }
      //pollInterval: REFRESH
    });

    return this._offersWatchQuery;
  }

  /**
   * Get one Offer (by ID) from DB.
   */
  private _offerWatchQuery: QueryRef<any, EmptyObject>;

  qGetOffer(userId: any): QueryRef<any, EmptyObject> {
    this._offerWatchQuery = this.apollo.watchQuery({
      query: GET_OFFER,
      //pollInterval: REFRESH,
      variables: {
        id: userId,
      }
    });
    return this._offerWatchQuery;
  }

  /**
   * Get current candidatures of logged user
   */
  private _candidaturesWatchQuery: QueryRef<any, EmptyObject>;


  qGetMyCandidatures(): QueryRef<any, EmptyObject> {
    if (!this._candidaturesWatchQuery) {
      this._candidaturesWatchQuery = this.apollo.watchQuery({
        query: GET_CANDIDATURES
      });
    }
    return this._candidaturesWatchQuery;
  }

  qGetUploadOfferUrl(id: String, filename: String): Observable<any> {
    return this.apollo.query({
      query: GET_UPLOAD_FILE_URL,
      variables: {
        id,
        filename
      }
    })
  }

  qGetDownloadFile(id?: String, file?: String): Observable<any> {
    return this.apollo.query({
      query: GET_DOWNLOAD_FILE,
      variables: {
        id, 
        file
      }
    })
  }

  /**
   * Mutation to Create an Offer.
   * @returns new Offer
   */
  mCreateOffer(uId: any, iTitle: any, iEduLevel: any, iCompetence: any, iCity: any, iJornada: any, iRango: any, iRemoto: any,
    iEnroll: any, iContrato: any, iDescripcio: any, iRequirements: any, iDate: string, iRequiredExperience: RequiredExperience, status: OfferStatus, iFile: any) {
    return this.apollo.mutate({
      mutation: MUT_CREATEOFFER,
      variables: {
        userId: uId,
        title: iTitle,
        eduLevel: iEduLevel,
        competencies: iCompetence,
        requiredExperience: iRequiredExperience,
        city: iCity,
        workHours: iJornada,
        salaryRange: iRango,
        remote: iRemoto,
        enrolled: iEnroll,
        contractType: iContrato,
        description: iDescripcio,
        requirements: iRequirements,
        createdDate: iDate,
        status: status,
        file: iFile
      }
    }).pipe(
      map((data) => {
        this._offersWatchQuery?.refetch();
        return data;
      })
    );
  }

  /**
   * Mutation to Edit an Offer.
   * @returns edited Offer
   */
  mEditOffer(offerId: any, iTitle: any, iEduLevel: any, iCity: any, iJornada: any, iCompetence: any, iRango: any, iRemoto: any, iContrato: any, iDescripcio: any, iRequirements: any, iRequiredExperience: RequiredExperience, iFile: any) {
    return this.apollo.mutate({
      mutation: MUT_EDITOFFER,
      variables: {
        id: offerId,
        title: iTitle,
        eduLevel: iEduLevel,
        city: iCity,
        workHours: iJornada,
        competencies: iCompetence,
        requiredExperience: iRequiredExperience,
        salaryRange: iRango,
        remote: iRemoto,
        contractType: iContrato,
        description: iDescripcio,
        requirements: iRequirements,
        file: iFile
      }
    }).pipe(
      map((data) => {
        this._offersWatchQuery?.refetch();
      })
    );
  }

  mDeleteOffer(offerId:any){
    return this.apollo.mutate({
      mutation: MUT_DELETEOFFER,
      variables: {
        id: offerId
      }
    }).pipe(
      map((data) => {
        this._offersWatchQuery?.refetch();
      })
    )
  }

  mAddCandidate(oId: string, cId: string) {
    return this.apollo.mutate({
      mutation: MUT_UPDOFFER_ADD_CANDIDATE,
      variables: {
        offerId: oId,
        candidateId: cId
      }
    }).pipe(
      map((data) => {
        this._offerWatchQuery?.refetch();
        this._offersWatchQuery?.refetch();
        this._candidaturesWatchQuery?.refetch();
      })
    );
  }
  mRemoveCandidate(oId: string, cId: string) {
    return this.apollo.mutate({
      mutation: MUT_UPDOFFER_REMOVE_CANDIDATE,
      variables: {
        offerId: oId,
        candidateId: cId
      }
    }).pipe(
      map((data) => {
        this._offerWatchQuery?.refetch();
        this._offersWatchQuery?.refetch();
        this._candidaturesWatchQuery?.refetch();
      })
    );
  }

  mUpdateCandidateStatus(offerId: String, candidateId: String, status: String) {
    return this.apollo.mutate({
      mutation: MUT_UPDOFFER_UPDATE_CANDIDATE,
      variables: {
        offerId,
        candidateId,
        status
      }
    }).pipe(
      map(() => {
        this._offerWatchQuery?.refetch();
        this._offersWatchQuery?.refetch();
        this._candidaturesWatchQuery?.refetch();
      })
    )
  }

  mUpdateOfferStatus(offerId: String, status: String) {
    return this.apollo.mutate({
      mutation: MUT_UPDATE_OFFER_STATUS,
      variables: {
        offerId,
        status
      }
    }).pipe(
      map(() => {
        this._offerWatchQuery?.refetch();
        this._offersWatchQuery?.refetch();
        this._candidaturesWatchQuery?.refetch();
      })
    )
  }

  mEditOfferEnroll(offerId: any, updated: any) {
    console.log(updated);
    return this.apollo.mutate({
      mutation: MUT_UPDOFFER_ENROLL,
      variables: {
        id: offerId,
        updateValue: updated
      },
    }).pipe(
      map((data) => {
        this._offersWatchQuery?.refetch();
        this._offerWatchQuery?.refetch();
      })
    );
  }

}
