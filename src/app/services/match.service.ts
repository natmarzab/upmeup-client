import { Injectable } from "@angular/core";
import { Apollo, gql, QueryRef } from "apollo-angular";
import { EmptyObject } from "apollo-angular/types";
// import gql from "graphql-tag";
import { map } from "rxjs/operators";

const GET_MATCHES = gql `
    query getMatches ($id: String!){
        getMatches (userID: $id){
            _id
            userID
            externalID
            userType
            matchScore {
                competencies
                softSkills
                sectors
                city
                content
                total
            }
        }
    }
`

const MUT_CALCULATE_MATCH_BY_USER = gql`
    mutation calculateMatchMut($userID: String!){
        calculateMatchByUser(userID: $userID){
            calculatedMatches {
                uId
                eId
                match
            }
        }
    }
`

const MUT_CALCULATE_MATCH_BY_PROJECT = gql`
    mutation calculateMatchMut($projectID: String!){
        calculateMatchByProject(projectID: $projectID){
            calculatedMatches {
                uId
                eId
                match
            }
        }
    }
`

const MUT_DELETEPROJECTMATCHES = gql`
  mutation deleteProjectMatchesMut($projectID: String!) {
    deleteProjectMatches(projectID: $projectID) {
        calculatedMatches {
            uId
            eId
            match
        }
    }
  }`;

@Injectable({
    providedIn: 'root'
})
export class MatchService {
    constructor(private apollo: Apollo){}

    private _matchWatchQuery: QueryRef<any, EmptyObject>;

    qGetMatches(userID: string): QueryRef <any, EmptyObject>{
        this._matchWatchQuery = this.apollo.watchQuery({
            query: GET_MATCHES,
            variables: {
              id: userID
            }
          });
          return this._matchWatchQuery;
    }

    mCalculateMatchByUser(userID: string){
        return this.apollo.mutate({
            mutation: MUT_CALCULATE_MATCH_BY_USER,
            variables: {
                userID: userID
            }
        }).pipe(
            map((data) => {
                this._matchWatchQuery?.refetch();
            })
        )
    }

    mCalculateMatchByProject(projectID: string){
        return this.apollo.mutate({
            mutation: MUT_CALCULATE_MATCH_BY_PROJECT,
            variables: {
                projectID: projectID
            }
        }).pipe(
            map((data) => {
                this._matchWatchQuery?.refetch();
            })
        )
    }

    mDeleteProjectMatches(projectID: string){
        return this.apollo.mutate({
            mutation: MUT_DELETEPROJECTMATCHES,
            variables: {
              projectID: projectID
            }
          }).pipe(
            map((data) => {
              this._matchWatchQuery?.refetch();
            })
          )
    }
}
