/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable no-underscore-dangle */
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Route, Router, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // private currentUser: BehaviorSubject<any> = new BehaviorSubject(null);
  private _isLoggedIn = new BehaviorSubject(window.localStorage.getItem('token') ? true : false);
  userType = '0';

  constructor(private router: Router,
              private userService: UserService
              ) { }


  //TODO: Implement CanLoad to avoid a module to be loaded without Logging In.
  canLoad(route: Route): boolean {
    if (this.isLogged()) {
      return true;
    }
    return false;
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.isLogged()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }

  isLogged(){
    return this._isLoggedIn.value;
  }

  get isLoggedIn() {
    return this._isLoggedIn.asObservable();
  }

  onLogin() {
    this._isLoggedIn.next(true);
  }

  //LogOut app
  async onLogout() {
    // this.authToken = null;
    await sessionStorage.clear();
    await localStorage.clear();
    this._isLoggedIn.next(false);
    this.userService.forgetMe();
    window.location.assign('/');
  }

}
