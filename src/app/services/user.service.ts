/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { Apollo, gql, QueryRef } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


const GET_JWT = gql`
  query getJWTOfUser($email: String!, $password: String!) {
    getJWTByEmailAndPassword(email: $email, password: $password) {
      token
    }
  }
`;

const GET_USER = gql`
  query getOneUser($id: String!) {
    getUser(id: $id) {
      _id
      name
      surname
      eduLevel
      city
      website
      sector {
        _id
        name
      }
      email
      type
      jobPosition
      lastJobTasks
      experience
      languages
      softSkills {
        _id
        name
      }
      competencies {
        _id
        name
      }
      video
      cv
      coverLetter
      avatarB64
      rooms {
        userId
        roomId
      }
    }
  }
`;

const GET_USER_BY_EMAIL = gql`
  query getUserByEmail($email: String!) {
    getUserByEmail(email: $email) {
      _id
      email
    }
  }
`;

const GET_ME = gql`
  query {
    me {
      _id
      name
      surname
      eduLevel
      city
      website
      sector {
        _id
        name
      }
      email
      website
      type
      jobPosition
      lastJobTasks
      experience
      languages
      softSkills {
        _id
        name
      }
      competencies {
        _id
        name
      }
      video
      cv
      coverLetter
      avatarB64
      chatUserId
      chatAuthToken
      rooms {
        userId
        roomId
      }
    }
  }
`

const GET_CHAT_TOKEN = gql`
  query {
    getChatToken {
        chatUserId
        chatAuthToken
    }
  }
`

const GET_UPLOAD_FILE_URL = gql`
  query getUploadUrl($filename: String!) {
    uploadUrl(filename: $filename) {
      presignedUrl
    }
  }
`

const GET_DOWNLOAD_CV_URL = gql`
  query getDownloadCVUrl($id: String) {
    downloadCvUrl(id: $id) {
      presignedUrl
    }
  }
`

const GET_DOWNLOAD_COVER_LETTER_URL = gql`
  query getDownloadCoverLetterUrl($id: String) {
    downloadCoverLetterUrl(id: $id) {
      presignedUrl
    }
  }
`

const GET_ALLUSERS = gql`
  query {
    getUsers {
      _id
      name
      surname
      eduLevel
      city
      website
      sector {
        _id
        name
      }
      email
      type
      jobPosition
      lastJobTasks
      experience
      languages
      softSkills {
        _id
        name
      }
      competencies {
        _id
        name
      }
      video
    }
  }
`;

const MUT_EDIT_AVATAR = gql`
mutation editUserAvatar(
                          $id: String!,
                          $avatarB64: String!
                        ){
  updateUser(
    id: $id,
    userInputs: {
      avatarB64: $avatarB64
    }
  ) {
    _id
    name
    surname
    email
    city
    website
    sector {
      _id
      name
    }
    eduLevel
    jobPosition
    lastJobTasks
    experience
    languages
    softSkills {
      _id
      name
    }
    avatarB64
  }
}
`;

const MUT_EDIT_USERSKILLS = gql`
mutation editUserSkills(
                          $id: String!,
                          $softSkills: [EmbeddedSoftSkill!]
                        ) {

  updateUser(
    id: $id,
    userInputs: {
      softSkills: $softSkills
    }
  ) {
    _id
    name
    surname
    email
    city
    website
    sector {
      _id
      name
    }
    eduLevel
    jobPosition
    lastJobTasks
    experience
    languages
    softSkills {
      _id
      name
    }
  }
}
`;

const MUT_EDIT_PREFERENCES = gql`
mutation editUserPreferences(
                          $id: String!,
                          $preferences: PreferencesInput
                        ) {

  updateUser(
    id: $id,
    userInputs: {
      preferences: $preferences
    }
  ) {
    _id
    name
    surname
    email
    city
    website
    sector {
      _id
      name
    }
    eduLevel
    jobPosition
    lastJobTasks
    experience
    languages
    softSkills {
      _id
      name
    }
  }
}
`;

const MUT_EDIT_USER = gql`
mutation editUser (
                    $id: String!,
                    $name: String!,
                    $surname: String!,
                    $email: String!,
                    $city: String!,
                    $website: String,
                    $sector: [EmbeddedSector!]!,
                    $eduLevel: String!,
                    $jobPosition: String!,
                    $lastJobTasks: String!,
                    $experience: String!,
                    $languages: [String!]!,
                    $competencies: [EmbeddedCompetency!]!,
                    $video: String
                    $coverLetter: String
                  ) {

  updateUser(
    id: $id,
    userInputs: {
      name: $name
      surname: $surname
      email: $email
      city: $city
      website: $website
      sector: $sector
      eduLevel: $eduLevel
      jobPosition: $jobPosition
      lastJobTasks: $lastJobTasks
      experience: $experience
      languages: $languages
      competencies: $competencies
      video: $video
      coverLetter: $coverLetter
    }
  ) {
      _id
      name
      surname
      email
      city
      website
      sector {
        _id
        name
      }
      eduLevel
      jobPosition
      lastJobTasks
      experience
      languages
      competencies {
        _id
        name
      }
      video
    }
}
`;

const MUT_CREATEUSER = gql`
mutation createUserMut( $name: String!,
                        $surname: String!,
                        $email: String!,
                        $password: String!,
                        $type: String!,
                        $legalForm: LegalForm,
                        $city: String!,
                        $website: String,
                        $sector: [EmbeddedSector!]!,
                        $eduLevel: String!,
                        $jobPosition: String!,
                        $lastJobTasks: String!,
                        $experience: String!,
                        $languages: [String!]!,
                        $competencies: [EmbeddedCompetency!]!,
                        $softSkills: [EmbeddedSoftSkill!]!,
                        $commsOK: Boolean!,
                        $privacyPolicyOK: Boolean!) {
  createUser(
      createUserDto: { 
        name: $name
        surname: $surname
        email: $email
        password: $password
        type: $type
        legalForm: $legalForm
        city: $city
        website: $website
        sector: $sector
        eduLevel: $eduLevel
        jobPosition: $jobPosition
        lastJobTasks: $lastJobTasks
        experience: $experience
        languages: $languages
        competencies: $competencies
        softSkills: $softSkills
        commsOK: $commsOK
        privacyPolicyOK: $privacyPolicyOK
      }    
  ) {
      _id
      name
      surname
      email
      website
      type
      legalForm    
      city
      website
      sector {
        _id
        name
      }
      eduLevel
      jobPosition
      lastJobTasks
      experience
      languages
      competencies {
        _id
        name
      }
      softSkills {
        _id
        name
      }
      commsOK
      privacyPolicyOK
    }
}
`;

const MUT_CHECK_EMAIL = gql`
  mutation checkEmail($email: String!) {
    checkEmail(email: $email) {
      email
    }
  }
`

const MUT_UPDATE_CV = gql`
  mutation updateCV($filename: String) {
    cv(filename: $filename) {
      _id
      cv
    }
  }
`

const MUT_UPDATE_COVER_LETTER = gql`
  mutation updateCoverLetter($filename: String) {
    coverLetter(filename: $filename) {
      _id
      coverLetter
    }
  }
`

const MUT_GENERATE_PASSWORD_RESET_TOKEN = gql`
  mutation generatePasswordResetToken($email: String!){
    resetToken(email: $email) 
  }
`

const MUT_RESET_PASSWORD = gql`
  mutation resetPasswordMut($token: String!, $password: String!) {
    resetPassword(token: $token, password: $password)
  }
`

const MUT_CREATE_CHAT = gql`
  mutation createChatMut($loggedUserID: String!, $interlocutorID: String!) {
    createChatRoomIfNotExists(loggedUserID: $loggedUserID, interlocutorID: $interlocutorID)
  }
`

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private apollo: Apollo) { }

  /**
   * Get Users Login from DB.
   */
  private _allUsersWatchQuery: QueryRef<any, EmptyObject>;

  /*qGetUsersLog(): QueryRef<any, EmptyObject> {
    this._allUsersWatchQuery = this.apollo.watchQuery({
        query: GET_USERLOGIN
    });

    return this._allUsersWatchQuery;
  }*/

  qGetAllUsers(): QueryRef<any, EmptyObject> {
    this._allUsersWatchQuery = this.apollo.watchQuery({
      query: GET_ALLUSERS
    });

    return this._allUsersWatchQuery;
  }

  /**
   * Get one User (by ID) from DB.
   */
  private _oneUserWatchQuery: QueryRef<any, EmptyObject>;

  qGetUser(userId: any): QueryRef<any, EmptyObject> {
    this._oneUserWatchQuery = this.apollo.watchQuery({
      query: GET_USER,
      variables: {
        id: userId,
      }
    });
    return this._oneUserWatchQuery;
  }

  qGetUserByEmail(email: any): QueryRef<any, EmptyObject> {
    this._oneUserWatchQuery = this.apollo.watchQuery({
      query: GET_USER_BY_EMAIL,
      variables: {
        email: email
      }
    });
    return this._oneUserWatchQuery;
  }

  private _meWatchQuery: QueryRef<any, EmptyObject>;
  private _tokenWatchQuery: QueryRef<any, EmptyObject>;

  qGetMe(): QueryRef<any, EmptyObject> {
    if (!this._meWatchQuery) {
      this._meWatchQuery = this.apollo.watchQuery({
        query: GET_ME
      });
    }
    return this._meWatchQuery;
  }

  qGetChatToken(): QueryRef<any, EmptyObject> {
    if (!this._tokenWatchQuery) {
      this._tokenWatchQuery = this.apollo.watchQuery({
        query: GET_CHAT_TOKEN
      });
    }
    return this._tokenWatchQuery;
  }

  forgetMe() {
    this._meWatchQuery = undefined;
  }

  qGetJWT(email: String, password: String): Observable<any> {
    return this.apollo.query({
      query: GET_JWT,
      variables: {
        email,
        password
      }
    });
  }

  qGetUploadUrl(filename: String): Observable<any> {
    return this.apollo.query({
      query: GET_UPLOAD_FILE_URL,
      variables: {
        filename
      }
    })
  }

  qGetDownloadCVUrl(id?: String): Observable<any> {
    return this.apollo.query({
      query: GET_DOWNLOAD_CV_URL,
      variables: {
        id
      }
    })
  }

  qGetDownloadCoverLetterUrl(id?: String): Observable<any> {
    return this.apollo.query({
      query: GET_DOWNLOAD_COVER_LETTER_URL,
      variables: {
        id
      }
    })
  }

  mUserAvatarUpdate(userId, avatarB64: string) {

    return this.apollo.mutate({
      mutation: MUT_EDIT_AVATAR,
      variables: {
        id: userId,
        avatarB64: avatarB64
      }
    }).pipe(
      map(() => {
        this._oneUserWatchQuery?.refetch();
        this._meWatchQuery?.refetch();
      })
    );

  }

  mUserSkillUpdate(userId, skillsList) {

    return this.apollo.mutate({
      mutation: MUT_EDIT_USERSKILLS,
      variables: {
        id: userId,
        softSkills: skillsList
      }
    }).pipe(
      map(() => {
        this._oneUserWatchQuery?.refetch();
        this._meWatchQuery?.refetch();
      })
    );

  }

  mUserPreferencesUpdate(userId, preferences) {

    return this.apollo.mutate({
      mutation: MUT_EDIT_PREFERENCES,
      variables: {
        id: userId,
        preferences: preferences
      }
    }).pipe(
      map(() => {
        this._oneUserWatchQuery?.refetch();
        this._meWatchQuery?.refetch();
      })
    );

  }

  /**
  * Mutation to Edit user profile
  */
  mEditUser(userId, iName: any, iSurname: any, iEmail: any, iCity: any, iWebsite: any, iSector: any, iEduc: any,
    iJobPos: any, iLastJob: any, iExp: any, iLang: any, iCompetence: any, iVideo: any, iCoverLetter) {
    return this.apollo.mutate({
      mutation: MUT_EDIT_USER,
      variables: {
        id: userId,
        name: iName,
        surname: iSurname,
        email: iEmail,
        city: iCity,
        website: iWebsite,
        sector: iSector,
        eduLevel: iEduc,
        jobPosition: iJobPos,
        lastJobTasks: iLastJob,
        experience: iExp,
        languages: iLang,
        competencies: iCompetence,
        video: iVideo,
        coverLetter: iCoverLetter
      }
    }).pipe(
      map((data) => {
        this._oneUserWatchQuery?.refetch();
        this._meWatchQuery?.refetch();
      })
    );
  }

  /**
   * Mutation to create a new user
   */
  mCreateUser(iName: any, iSurname: any, iLegal: any, iCity: any, iWebsite: any, iSector: any, iEduc: any, iPassw: any, iType: any, iEmail: any, iJobPos: any, iLastJob: any, iExp: any, iLang: any, iCompetence: any, iValue: any, iComms: any, iPolicy) {
    return this.apollo.mutate({
      mutation: MUT_CREATEUSER,
      variables: {
        name: iName,
        surname: iSurname,
        legalForm: iLegal,
        city: iCity,
        website: iWebsite,
        sector: iSector,
        eduLevel: iEduc,
        password: iPassw,
        type: iType,
        email: iEmail,
        jobPosition: iJobPos,
        lastJobTasks: iLastJob,
        experience: iExp,
        languages: iLang,
        competencies: iCompetence,
        softSkills: iValue,
        commsOK: iComms,
        privacyPolicyOK: iPolicy
      }
    }).pipe(
      map((data) => {
        this._allUsersWatchQuery?.refetch();
      })
    );
  }

  mCheckEmail(iEmail: any) {
    return this.apollo.mutate({
      mutation: MUT_CHECK_EMAIL,
      variables: {
        email: iEmail
      }
    }).pipe(
      map((data) => {
        this._allUsersWatchQuery?.refetch();
      })
    );
  }

  mUpdateCV(filename: String) {
    return this.apollo.mutate({
      mutation: MUT_UPDATE_CV,
      variables: {
        filename: filename
      }
    }).pipe(
      map((data) => {
        this._oneUserWatchQuery?.refetch();
        this._meWatchQuery?.refetch();
      })
    );
  }

  mUpdateCoverLetter(filename: String) {
    return this.apollo.mutate({
      mutation: MUT_UPDATE_COVER_LETTER,
      variables: {
        filename: filename
      }
    }).pipe(
      map((data) => {
        this._oneUserWatchQuery?.refetch();
        this._meWatchQuery?.refetch();
      })
    );
  }

  mGeneratePasswordResetToken(email: String): Observable<any> {
    return this.apollo.mutate({
      mutation: MUT_GENERATE_PASSWORD_RESET_TOKEN,
      variables: {
        email: email
      }
    }).pipe(
      map((data) => {
        data
      })
    );
  }

  mResetPassword(token: String, password: String): Observable<any> {
    return this.apollo.mutate({
      mutation: MUT_RESET_PASSWORD,
      variables: {
        token,
        password
      }
    }).pipe(
      map((data) => {
        data
      })
    );
  }

  mCreateChatRoomIfNotExists(loggedUserID: string, interlocutorID: string): Observable<any> {
    return this.apollo.mutate({
      mutation: MUT_CREATE_CHAT,
      variables: {
        loggedUserID,
        interlocutorID
      }
    }).pipe(
      map((data) => {
        data
        this._oneUserWatchQuery?.refetch();
        this._tokenWatchQuery?.refetch();
      })
    );
  };

}
