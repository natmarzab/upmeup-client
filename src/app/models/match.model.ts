

export class Match {
    _id: string;
    userID: string;
    externalID: string;
    userType: string;
    matchScore: MatchScore;
}

export class MatchScore {
    competencies: number;
    softSkills: number;
    content: number;
    experience: number;
    sectors: number;
    city: number;
    total: number;
}
