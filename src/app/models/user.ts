import { Candidatures } from "./candidatures.model";
import { Competency } from "./competency.model";
import { Room } from "./room";
import { Sector } from "./sector.model";
import { SoftSkill } from "./soft-skill.model";

/* eslint-disable @typescript-eslint/naming-convention */
export class User {
     _id: string;
     name: string;
     surname: string;
     email: string;
     website: string;
     eduLevel: string;
     city: string;
     sector: Array<Sector>;
     experience: string;
     jobPosition: string;
     lastJobTasks: string;
     languages: Array<string>;
     type: string;
     competencies: Array<Competency>;
     softSkills: Array<SoftSkill>;
     avatarB64: string;
     coverLetter: string;
     cv: string;
     video: string;
     commsOK: Boolean;
     privacyPolicyOK: Boolean;

     chatUserId: string;
     chatAuthToken: string;
     rooms: Array<Room>;
}

