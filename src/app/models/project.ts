import { Competency } from "./competency.model";
import { InterestedUser } from "./interested-user.model";
import { OfferStatus } from "./offer-status.model";
import { ProjectStatus } from "./project-status.model";
import { Sector } from "./sector.model";
import { SoftSkill } from "./soft-skill.model";
import { User } from "./user";


export class Project {
        _id: string;
        userId: string;
        title: string;
        description: string;
        sector: Array<Sector>;
        competencies: Array<Competency>;
        city: string;
        user: User;
        softSkills: Array<SoftSkill>;
        status: ProjectStatus;
        createdDate: string;
        files: Array<string>;
        interestedUsers: Array<InterestedUser>;
        state: OfferStatus;
        admins: Array<Member>;
        
}

export class Member {
        _id: string;
        email: string;
}
