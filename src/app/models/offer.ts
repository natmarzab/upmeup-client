import { CandidatureStatus } from "./candidature-status.model";
import { Competency } from "./competency.model";
import { OfferStatus } from "./offer-status.model";
import { RequiredExperience } from "./required-experience.model";
import { User } from "./user";

/* eslint-disable @typescript-eslint/naming-convention */
export class Offer {
        _id: string;
        userId: string;
        title: string;
        eduLevel: string;
        city: string;
        workHours: string;
        salaryRange: string;
        remote: string;
        enrolled: number;
        contractType: string;
        description: string;
        createdDate: string;
        competencies: Array<Competency>;
        match: number;
        user: User;
        candidates: Array<Candidate>;
        requiredExperience: RequiredExperience;
        requirements: string; 
        status: OfferStatus;
        file: Array<string>;
        
}

export class Candidate {
        user: User;
        status: CandidatureStatus;
        match: OfferMatch;
}

export class OfferMatch {
        total: number;
}