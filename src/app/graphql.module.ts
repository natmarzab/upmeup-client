/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { HttpErrorResponse } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { ApolloLink, InMemoryCache } from '@apollo/client/core';
import { setContext } from '@apollo/client/link/context';
import { onError } from '@apollo/client/link/error';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { environment } from 'src/environments/environment';

// Connection to Data
export function createApollo(httpLink: HttpLink) {
    const uri = environment.API_URL + '/graphql'
    const basic = setContext((operation, context) => ({
      headers: {
        Accept: 'charset=utf-8'
      }
    }));
   
    const auth = setContext((operation, context) => {
      const token = localStorage.getItem('token');
   
      if (token === null) {
        return {};
      } else {
        return {
          headers: {
            Authorization: `Bearer ${token}`
          }
        };
      }
    });

    const error = onError(({ graphQLErrors, networkError }) => {
      if (graphQLErrors && graphQLErrors.find(e => e.extensions?.exception?.status === 401 || e.extensions.response?.statusCode === 401)) {
        sessionStorage.clear();
        localStorage.clear();
        window.location.assign('/');
      }

      const networkErrorRef:HttpErrorResponse = networkError as HttpErrorResponse;
      if (networkErrorRef && networkErrorRef.status === 401) {
        sessionStorage.clear();
        localStorage.clear();
        window.location.assign('/');
      }
    });

    return {
      link: ApolloLink.from([basic, auth, error, httpLink.create({ uri })]),
      cache: new InMemoryCache(),
    };

}

@NgModule({
  exports: [ ApolloModule ],
  providers: [{
    provide: APOLLO_OPTIONS,
    useFactory: createApollo,
    deps: [HttpLink],
  }],
})
export class GraphQLModule {}
