import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path:'password-reset',
    loadChildren: () => import('./pages/password-reset/password-reset.module').then(m => m.PasswordResetModule)
  },
  {
    path:'password-reset/:token',
    loadChildren: () => import('./pages/password-reset/password-reset.module').then(m => m.PasswordResetModule)
  },
  {
    path: 'offer-list',
    loadChildren: () => import('./pages/offer-list/offer-list.module').then( m => m.OfferListPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'offer-detail/:id',
    loadChildren: () => import('./pages/offer-detail/offer-detail.module').then( m => m.OfferDetailPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./pages/user-profile/user-profile.module').then( m => m.UserProfilePageModule),
    canActivate: [AuthService]
  },
  {
    path: 'entity-list',
    loadChildren: () => import('./pages/entity-list/entity-list.module').then( m => m.EntityListPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'values-modal',
    loadChildren: () => import('./shared/modals/values-modal/values-modal.module').then( m => m.ValuesModalPageModule),
  },
  {
    path: 'company-profile',
    loadChildren: () => import('./pages/company-profile/company-profile.module').then( m => m.CompanyProfilePageModule),
    canActivate: [AuthService]
  },
  {
    path: 'company-offer',
    loadChildren: () => import('./pages/company-offer/company-offer.module').then( m => m.CompanyOfferPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'company-offer-detail/:id',
    loadChildren: () => import('./pages/company-offer-detail/company-offer-detail.module').then( m => m.CompanyOfferDetailPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'candidatures',
    loadChildren: () => import('./pages/candidatures/candidatures.module').then( m => m.CandidaturesPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'edit-user',
    loadChildren: () => import('./pages/edit-user/edit-user.module').then( m => m.EditUserPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'edit-offer',
    loadChildren: () => import('./pages/edit-offer/edit-offer.module').then(m => m.EditOfferPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'new-user',
    loadChildren: () => import('./pages/marketing/new-user/new-user/new-user.module').then(m => m.NewUserComponentModule)
  },
  {
    path: 'mp1',
    loadChildren: () => import('./pages/marketing/mp1/mp1.module').then(m => m.Mp1ComponentModule)
  },
  {
    path: 'create-offer',
    loadChildren: () => import('./pages/create-offer/create-offer.module').then(m => m.CreateOfferPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'menu-mobile',
    loadChildren: () => import('./shared/menu-mobile/menu-mobile.module').then(m => m.MenuMobileModule),
    canActivate: [AuthService]
  },
  {
    path: 'help',
    loadChildren: () => import('./pages/help/help.module').then(m => m.HelpPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'esborrar_compta',
    loadChildren: () => import('./pages/delete-account/delete-account.module').then(m => m.DeleteAccountPageModule),
    canActivate: [AuthService]
  },
  {
    path: 'legal-info',
    loadChildren: () => import('./pages/legal-info/legal-info.module').then(m => m.LegalInfoPageModule)
  },
  {
    path: 'project-list',
    loadChildren: () => import('./enterprising/project-list/project-list.module').then(m => m.ProjectListComponentModule)
  },
  {
    path: 'create-project',
    loadChildren: () => import('./enterprising/create-project/create-project.module').then(m => m.CreateProjectComponentModule)
  },
  {
    path: 'interest-projects',
    loadChildren: () => import('./enterprising/interest-projects/interest-projects.module').then(m => m.InterestProjectsComponentModule)
  },
  {
    path: 'project-detail/:id',
    loadChildren: () => import('./enterprising/project-detail/project-detail.module').then(m => m.ProjectDetailComponentModule)
  },
  {
    path: 'edit-project',
    loadChildren: () => import('./enterprising/edit-project/edit-project.module').then(m => m.EditProjectComponentModule)
  },
  {
    path: 'all-projects-list',
    loadChildren: () => import('./enterprising/all-projects-list/all-projects-list.module').then(m => m.AllProjectsListComponentModule)
  },
  {
    path: 'interested-entity',
    loadChildren: () => import('./pages/interested-entity/interested-entity.module').then(m => m.InterestedEntityComponentModule)

  },
  {
    path: 'chat/:chatId',
    loadChildren: () => import('./pages/chats/chat.module').then(m => m.ChatPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
