import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgxPwaInstallModule } from 'ngx-pwa-install';
import { environment } from '../environments/environment';
import { GraphQLModule } from './graphql.module';
import { AuthService } from './services/auth.service';

// import ngx translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CompanyOfferPage } from './pages/company-offer/company-offer.page';
import { MultilanguageComponent } from './shared/multilanguage/multilanguage.component';
import { MultilanguageModule } from './shared/multilanguage/multilanguage.module';
import { ModalModule } from 'ngx-bootstrap/modal';




@NgModule({
    declarations: [AppComponent],
    imports: [
        MultilanguageModule,
        BrowserModule, 
        BrowserAnimationsModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        GraphQLModule,
        ModalModule.forRoot(),
        NgxPwaInstallModule.forRoot(),
        ServiceWorkerModule.register('ngsw-worker.js', {
          enabled: environment.production,
          // Register the ServiceWorker as soon as the application is stable
          // or after 30 seconds (whichever comes first).
          // registrationStrategy: 'registerWhenStable:30000'
        }),
        //ngx-translate and loader module
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ModalModule.forRoot()
        
    ],
    providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, AuthService, MultilanguageComponent, CompanyOfferPage],
    bootstrap: [AppComponent],
})
export class AppModule {
    // constructor(httpClient: HttpClient) {}
}

//required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
    return new TranslateHttpLoader(http);
}