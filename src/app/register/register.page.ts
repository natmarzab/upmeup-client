/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CheckboxCustomEvent, IonContent, IonModal, IonRouterOutlet, IonSlides, LoadingController, MenuController, ModalController, ToastController } from '@ionic/angular';


import { map } from 'rxjs/operators';
import Validation from './../utils/validation';

import { Router } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { LegalForm } from '../models/legal-form.model';
import { AuthService } from '../services/auth.service';
import { CompetenceService } from '../services/competence.service';
import { SectorsService } from '../services/sectors.service';
import { SoftskillsService } from '../services/softskills.service';
import { UserService } from '../services/user.service';
import { ValuesModalPage } from '../shared/modals/values-modal/values-modal.page';
import { MultilanguageComponent } from '../shared/multilanguage/multilanguage.component';
import { cityList, languageList } from '../utils/constants';
import { MatchService } from '../services/match.service';


interface sector {
  _id: string,
  name: string
}


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  
  readonly languageList = languageList;
  readonly cityList = cityList;
  readonly legalFormList = Object.values(LegalForm);
  steps = ['access', 'basic-data', 'password', 'sectors', 'description', 'values']

  emailRegex = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,10}$';
  passwordRegex = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$';
  skillsListId: any[] = [];
  sectorList: any[] = [];
  registerForm: FormGroup;
  isSubmitted = false;
  userType = '2';
  passw = '';
  userSkills: any[] = [];
  userSkillsIds: Set<string> = new Set(); 
  competList: any[] = [];
  competenciesNames: string[] = [];
  userCompets: any[] = [];
  userCompetsIds: any[] = [];
  nameNewCompet: any = [];
  createdCompet: any = [];
  selectedLang = "";
  languageListTranslated: any[] = [];
  finalLangList: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  sectorListTranslated: any[] = [];
  finalSectorList: any[] = []; 
  usectors: sector[] = [];
  userExist = false;
  skillsList: any[] = [];
  finalSkillsList: { name: any, output: any, _id: any, label: any }[] = [{ "name": "", "_id": "", "output": "", "label": "" }];
  newUser = false;
  error : any;
  errorMessage: any;

  // legalInfoText : any = LegalInfoPageModule;
  // legalInfoText : LegalInfoPage = LegalInfoPage;
  // legalInfoText : any = 'Most basic test';

  slideOpts = {
    allowTouchMove: false,
    scrollbar: true
  };
  formSlide1: FormGroup;
  formSlide2: FormGroup;
  formSlide3: FormGroup;
  formSlide4: FormGroup;
  formSlide5: FormGroup;

  @ViewChild('registerSlider') slides: IonSlides;

  constructor(
    private menu: MenuController,
    private modalController: ModalController,
    private toastCtrl: ToastController,
    private routerOutlet: IonRouterOutlet,
    private router: Router,
    public fBuilder: FormBuilder,
    private uService: UserService,
    private sectorService: SectorsService,
    private competService: CompetenceService,
    private softSkillService: SoftskillsService,
    public loadingCtrl: LoadingController,
    private translateList: MultilanguageComponent,
    private auth: AuthService,
    private mService: MatchService
    
  ) { }
  
  // Slides  
  public curSlide: number = 0
    
    
    

  // Init / load logic
  async ngOnInit() {
    // console.log(`init register.page`)
    
    sessionStorage.clear();
    this.menu.enable(false);
    this.qGetSectorList();
    this.qsoftSkillsQuery();
    this.qGetCompetencies();
    this.createFormSlide1();
    this.createFormSlide2();
    this.createFormSlide3();
    this.createFormSlide4();
    this.createFormSlide5();
    
    this.finalCityList = this.translateList.translateCityList();
    this.finalLangList = this.translateList.translateLangList();
    

  }

  // Get sectors list
  qGetSectorList() {
    this.sectorService.qGetAllSectors().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sectorList = item.getSectors;
    });
  }

  /**
 * Get softSkills from DB.
 */
  qsoftSkillsQuery() {
    this.softSkillService.qGetAllSkills().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.skillsList = item.getSoftskills;
      this.finalSkillsList = this.translateList.translateSkillsLists(this.skillsList)
    });
  }


  getType(selected) {
    if (selected) {
      
      if(this.userType){
        this.userType = selected;
        this.curSlide = 0
      }
      else {
        this.userType = selected;
      }     
      if (selected == '1' && this.steps.length < 7) this.steps.push('studies');
        else if (selected == '2' && this.steps.length == 7) this.steps.pop();
      this.swipeNext();
    }

    // Init form according to Type
    this.createFormSlide1();
    this.createFormSlide2();
    this.createFormSlide3();
    this.createFormSlide4();
    this.createFormSlide5();

    this.finalSectorList = this.translateList.translateSectorList(this.sectorList);

  }

  // Forms slides
  createFormSlide1() {
    if (this.userType === '2') { // company
      this.formSlide1 = this.fBuilder.group({
        iName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
        iCity: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]],
        iLegal: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]]
      });
    } else if (this.userType === '1') {
      this.formSlide1 = this.fBuilder.group({
        iName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
        iSurname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
        iCity: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(80)]]
      });
    }
  }

  createFormSlide2() {
    this.formSlide2 = this.fBuilder.group({
      iEmail: ['', [Validators.required, Validators.minLength(8), Validators.pattern(this.emailRegex)]],
      iPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(64), Validators.pattern(this.passwordRegex)])],
      confirmPassword: ['', Validators.required]
    },
      {
        validators: [Validation.match('iPassword', 'confirmPassword')]
      });
  }

  createFormSlide3() {
    if (this.userType === '2') { // company
      this.formSlide3 = this.fBuilder.group({
        iWebsite: ['', [Validators.minLength(0), Validators.maxLength(100)]],
        iSector: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(40)]],
        iExp: [0, [Validators.minLength(1), Validators.maxLength(2)]]
      });
    } else if (this.userType === '1') { // candidate
      this.formSlide3 = this.fBuilder.group({
        iJobPos: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(70)]],
        iSector: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(40)]],
        iExp: [0, [Validators.minLength(1), Validators.maxLength(2)]],
      });
    }
  }

  createFormSlide4() {
    this.formSlide4 = this.fBuilder.group({
      iLastJob: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(1000)]]
    });
  }

  // Get competencies
  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => {
        this.competList = result.data.getCompetencies;
        this.competenciesNames = [...this.competList].filter(compet => compet.name).sort((a, b) => this.translateList.translate.instant(a.name) > this.translateList.translate.instant(b.name) ? 1 : -1).map(c => c.name);
      })
    ).subscribe((item) => {
      // console.log(this.competList);
    });
  }

  addCompetency(competency) {
    if(!this.userCompets) this.userCompets = [];
    this.userCompets.push(competency)
  }

  removeCompetency(competency) {
    this.userCompets = this.userCompets.filter(c => c !== competency);
  }

  createFormSlide5() {
 
      this.formSlide5 = this.fBuilder.group({
        iEduc: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
        iLang: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(10)]],
      });
    
  }

  validationForm1(){
    if (!this.formSlide1.valid){
      this.error = 'true';
    }
    else {
      this.submitForm()
      this.error = 'false';
    }
  }

  validationForm2(){
    if (!this.formSlide2.valid){
      this.error = 'true';
    }
    else {
      this.uService.mCheckEmail(this.formSlide2.value.iEmail)
      .subscribe({
        next: () => {
          this.loadingCtrl.dismiss();
          this.submitForm()
          this.error = 'false';
          return;
        },
        error: async (error) => {
          this.loadingCtrl.dismiss();
          this.translateList.translate.stream(error.message).subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'danger',
              message: value,
              duration: 3000,
              position: 'bottom'
            });

            toast.present();
            return this.errorMessage;
          });
        }
      });


      
    }
  }

  validationForm3(){
    if (!this.formSlide3.valid){
      this.error = 'true';
    }
    else {
      this.submitForm()
      this.error = 'false';
    }
  }

  validationForm4(){
    if (!this.formSlide4.valid){
      this.error = 'true';
    }
    else {
      this.submitForm()
      this.error = 'false';
    }
  }

  // Finish
  finish(){
    console.log(this.userType)
    if (this.userType === '1'){
      if (!this.formSlide5.valid || this.privacyPolicyOK !== true || this.userCompets.length < 2){
        this.error = 'true';
      }
      else if (this.formSlide5.valid && this.userCompets.length >= 2 && this.privacyPolicyOK == true){
        this.saveNewUser()
      }
    }
    else if (this.userType === '2'){
      if (this.privacyPolicyOK !== true){
        this.error = 'true';
      }
      else if (this.privacyPolicyOK == true){
        this.saveNewUser()
      }
    }
    
    
  }

  // Privacy policy modal area
  @ViewChild(IonModal) modal: IonModal;

  name: string;
  commsOK : Boolean = false;
  privacyPolicyOK : Boolean = false;
  
  onCommsChanged(event: Event) {
    const ev = event as CheckboxCustomEvent;
    this.commsOK = ev.detail.checked;
  }

  onPrivacyChanged(event: Event) {
    const ev = event as CheckboxCustomEvent;
    this.privacyPolicyOK = ev.detail.checked;
  }
  
  // OK modal
  confirm() {
    // console.log(`modal : confirm ()`)
    this.modal.dismiss(this.name, 'confirm');
  }
  
  hover() {
    // console.log(`register : finish : hover ()`)
    if ( !this.commsOK) {
        // Display message
    }
    
  }
  
  submitForm() {
    console.log(this.formSlide1.value);
    console.log(this.formSlide2.value);
    console.log(this.formSlide3.value);
    console.log(this.formSlide4.value);
    console.log(this.formSlide5.value);
    
    this.swipeNext();
    this.selectedLang = window.localStorage.getItem('language');
  }

  // POST to DB - Create user
  createNewUser(iName: any, iSurname: any, iLegal: any, iCity: any, iWebsite: any, iSector: any, iEduc: any, iPassw: any, iType: any, iEmail: any, iJobPos: any, iLastJob: any, iExp: any, iLang: any, iCompetence: any, iSkills: any, iComms: any, iPolicy: any) {
    this.uService.mCreateUser(iName, iSurname, iLegal, iCity, iWebsite, iSector, iEduc, iPassw, iType, iEmail, iJobPos, iLastJob, iExp, iLang, iCompetence, iSkills, iComms, iPolicy)
      .subscribe({
        next: () => {
          this.loadingCtrl.dismiss();
          this.loginIn(this.formSlide2.value.iEmail, this.formSlide2.value.iPassword);
        },
        error: async (error) => {
          this.loadingCtrl.dismiss();
          this.translateList.translate.stream(error.message).subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'danger',
              message: value,
              duration: 3000,
              position: 'bottom'
            });

            toast.present();
          });
        }
      });
  }

  loginIn(uEmail, uPassword) {
    this.uService.qGetJWT(uEmail, uPassword).subscribe(({ data: { getJWTByEmailAndPassword: { token } } }) => {
      if (token.length > 0) {
        localStorage.setItem('token', token);
        this.auth.onLogin();
        this.newUser = true; 
        this.uService.qGetMe().valueChanges.subscribe(response => {
          const loggedUser = response.data.me;
          this.userExist = true;
          this.useSessionStorage(loggedUser._id, loggedUser.name, loggedUser.email, loggedUser.type, loggedUser.softSkills.map(s => s.name));
          this.userType = loggedUser.type;

          this.calculateMatch(loggedUser._id);

          if (this.newUser === true){
            this.router.navigateByUrl('/new-user', { replaceUrl: true });
          }

          if (window.sessionStorage) {
            sessionStorage.removeItem('uSelectedSkills');
          }

          this.menu.enable(true);
          this.isSubmitted = true;
          this.newUser = false; 
          return;
        })
      } else { this.isSubmitted = true }
    });
  }
  
  useSessionStorage(uid, uName, uMail, uType, uSkills) {
    sessionStorage.setItem('userid', uid);
    sessionStorage.setItem('user', uName);
    sessionStorage.setItem('email', uMail);
    sessionStorage.setItem('type', uType);
    sessionStorage.setItem('uSelectedSkills', uSkills);
  }

  calculateMatch(userID){
    this.mService.mCalculateMatchByUser(userID).subscribe(() => {
      console.log('Match calculated!');
    });

  }

  // Save user info
  async saveNewUser() {

    if (this.userType === '1') { // Candidate
      await this.insertedTags(this.userCompets);
    } else { // Company
      this.userCompetsIds = [];
      this.formSlide1.value.iSurname = '-';
      this.formSlide3.value.iJobPos = '-';
      this.formSlide5.value.iEduc = '-';
      this.formSlide5.value.iLang = [];
    }

    this.loadingCtrl.create({
      message: 'Creant nou usuari...'
    }).then(async res => {
      res.present();
      console.log('Guardando info...!');


      this.formSlide3.value.iSector.forEach(usector => {
        const { _id, name } = this.sectorList.find(sector => usector === sector._id)
        this.usectors.push({ _id, name })
      })

      this.createNewUser(
        this.formSlide1.value.iName,
        this.formSlide1.value.iSurname,
        this.formSlide1.value.iLegal,
        this.formSlide1.value.iCity,
        this.formSlide3.value.iWebsite,
        this.usectors,
        this.formSlide5.value.iEduc,
        this.formSlide2.value.iPassword,
        this.userType,
        this.formSlide2.value.iEmail,
        this.formSlide3.value.iJobPos,
        this.formSlide4.value.iLastJob,
        this.formSlide3.value.iExp + "",
        this.formSlide5.value.iLang,
        this.userCompetsIds,
        this.userSkills,
        this.commsOK,
        this.privacyPolicyOK
      );
    });
  }

  // Created new competence if not exist yet
  async insertedTags(competencies) {

    for (let i=0; i<competencies.length; i++) {
      const competency = this.competList.find(c => c.name.trim().toLowerCase() === competencies[i].trim().toLowerCase())
      if (!competency) {
        const newCompetency = await this.createNewCompetence(competencies[i]);
        console.log(newCompetency);
        this.userCompetsIds.push({ "_id": newCompetency._id, "name": newCompetency.name });
      } else {
        this.userCompetsIds.push({ "_id": competency._id, "name": competency.name });
      }
    }
  }

  // Create new competence:
  async createNewCompetence(iName: any) {
    return firstValueFrom(this.competService.mCreateCompetence(iName))
  }

  addExperience(experience: number) {
    if (+this.formSlide3.value.iExp > 0 || experience > 0)
      this.formSlide3.controls['iExp'].setValue((+this.formSlide3.value.iExp) + experience);
  }

  toggleSkill(skillId: string) {
    if (this.userSkillsIds.has(skillId)) {
      this.userSkillsIds.delete(skillId);
      this.userSkills = this.userSkills.filter(s => s._id !== skillId);
    } else {
      const skill = this.skillsList.find(s => s._id === skillId);
      if (skill) {
        this.userSkillsIds.add(skillId);
        this.userSkills.push({ _id: skill._id, name: skill.name });
      }
    }
    console.log(this.userSkillsIds, this.userSkills);
  }

  // Find id of new competencies:
  findCompetence(names) {
    let pendingNames = [];
    names.forEach(el => {
      const index = this.competList.findIndex(
        object => object.name.trim().toLowerCase() === el.trim().toLowerCase()
      );

      if (index === -1) {
        console.log('No se encuentra competència!!');
        pendingNames.push(el);
      } else {
        this.createdCompet.push(this.competList[index]);
        this.userCompetsIds.push({ "_id": this.competList[index]._id, "name": this.competList[index].name });
      }
    });
    this.nameNewCompet = pendingNames;
  }

  // Delete function to competencies/tags.
  removeCompetence(item) {
    const tags = this.formSlide5.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  swipeNext() {
    this.content.scrollToTop(500);
    this.curSlide += 1;
    this.slides.slideNext(500);
  }

  swipeBack() {
    this.content.scrollToTop(500);
    this.curSlide -= 1;
    this.slides.slidePrev(500);
  }

  changeSlide(slide){
    if(slide <= this.curSlide){
      this.slides.slideTo(slide)
    } 
  }

  /**
   * Call modal to change values from profile.
   */
  async valuesModal() {
    console.log(`valuesModal!`)
    const modal = await this.modalController.create({
      component: ValuesModalPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });

    modal.onDidDismiss().then((infoMdl) => {
      console.log(`modal.onDidDismiss`)
      if (infoMdl.data !== undefined || window.sessionStorage) {
        this.userSkills = infoMdl.data;
      } else {
        this.userSkills = [];
      }
    });

    return await modal.present();
  }

  goToSlide(slide: number) {
    if (slide < this.curSlide || true) {
      this.curSlide = slide
      this.content.scrollToTop(500);
      this.slides.slideTo(slide);
    }
  }

  /**
   * Control Form Errors
  */
  get errorsSld1() {
    return this.formSlide1.controls;
  }

  get errorsSld2() {
    return this.formSlide2.controls;
  }

  get errorsSld3() {
    return this.formSlide3.controls;
  }

  get errorsSld4() {
    return this.formSlide4.controls;
  }

  get errorsSld5() {
    return this.formSlide5.controls;
  }

  showPass(){
    const password = document.querySelector("#iPassword");
    const type = password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
    
  }

  showConfirmedPass(){
    const password = document.querySelector("#confirmPassword");
    const type = password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
    
  }
  

}
