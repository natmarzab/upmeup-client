import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';
import { TagInputComponent } from '../components/tag-input/tag-input.component';
import { MultilanguageComponent } from '../shared/multilanguage/multilanguage.component';
import { MultilanguageModule } from '../shared/multilanguage/multilanguage.module';
import { RegisterPageRoutingModule } from './register-routing.module';
import { RegisterPage } from './register.page';
import { LegalInfoPageModule } from '../pages/legal-info/legal-info.module';


@NgModule({
  imports: [
      MultilanguageModule,
      CommonModule,
      // TagInputModule,
      FormsModule,
      TagInputModule,
      ReactiveFormsModule,
      IonicModule,
      RegisterPageRoutingModule,
      LegalInfoPageModule,
      //ngx-translate and loader module
      HttpClientModule,
      TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
      })
  ],
  declarations: [RegisterPage, TagInputComponent],
  providers: [MultilanguageComponent]
})
export class RegisterPageModule {}
