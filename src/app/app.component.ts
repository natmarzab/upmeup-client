import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs';
import { User } from './models/user';
import { CompanyOfferPage } from './pages/company-offer/company-offer.page';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';
import { MultilanguageComponent } from './shared/multilanguage/multilanguage.component';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent  {
  appPages = [];
  userName: string;
  userType: string;
  userInfo: User;
  avatar: string;

  constructor(
    public auth: AuthService,
    private router: Router,
    public menuCtrl: MenuController,
    public translate: TranslateService,
    public multilengague: MultilanguageComponent,
    private uService: UserService) {
      this.auth.isLoggedIn.subscribe(isLoggedIn => {
        if(isLoggedIn)
          this.qGetLoggedUser();
      })       
  };

  qGetLoggedUser() {    
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
      this.userName = item.me.name;
      this.userType = item.me.type;
      this.menuView(true, item.me.type);      
    });    
  }

  // Menu view
  menuView(res, userType) {
    if (res === true) {
      if (userType === '2') {
        this.appPages = [
          { title: 'profile', url: '/company-profile', icon: 'person' },
          { title: 'my-offers', url: '/company-offer', icon: 'briefcase' }
        ];
      } else if (userType === '1') {
        this.appPages = [
          { title: 'profile', url: '/user-profile', icon: 'person' },
          { title: 'offers', url: '/offer-list', icon: 'briefcase' }
        ];
      }
    }
  }

  logOut() {
    this.auth.onLogout();
    this.menuCtrl.enable(false);
    this.router.navigate(['/login']);
  }

}
