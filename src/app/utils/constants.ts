export const remoteOptions: string[] = ['attended', 'telecommuting', 'hybrid'];

export const contractTypeOptions: string[] = ['indefinite', 'permanent-discontinuous', 'fixed-term', 'formative'];

export const workHourOptions: string[] = ['full-time', 'part-time', 'split-day', 'continuous-workday', 'hourly-rate', 'reduced', 'night-shift', 'turns', 'holiday-only']

export const languageList: string[] = ['Arabic', 'Aranese', 'Basque', 'Bosnian', 'Catalan', 'Croatian', 'Danish', 'Dutch', 'English', 'French', 'Galician', 'German', 'Hindi', 'Italian', 'Japanese', 'Korean', 'Mandarin Chinese', 'Polish', 'Portuguese', 'Romanian', 'Russian', 'Serbian', 'Spanish', 'Turkish', 'Ukrainian', 'Valencian', 'Vietnamese'];

export const cityList: string[] = ['A Coruña', 'Alicante', 'Albacete', 'Almería', 'Asturias', 'Ávila', 'Araba', 'Badajoz', 'Barcelona', 'Burgos', 'Cantabria', 'Castellón', 'Ceuta', 'Ciudad real', 'Cuenca', 'Cáceres', 'Cádiz', 'Córdoba', 'El hierro', 'Formentera', 'Fuerteventura', 'Girona', 'Gran canaria', 'Granada', 'Guadalajara', 'Guipuzcoa', 'Huelva', 'Huesca', 'Ibiza', 'Jaén', 'La Gomera', 'La Palma', 'La Rioja', 'Lanzarote', 'León', 'Lleida', 'Lugo', 'Madrid', 'Mallorca', 'Menorca', 'Murcia', 'Málaga', 'Melilla', 'Navarra', 'Orense', 'Palencia', 'Pontevedra', 'Salamanca', 'Segovia', 'Sevilla', 'Soria', 'Tarragona', 'Tenerife', 'Teruel', 'Toledo', 'Valencia', 'Valladolid', 'Vizcaya', 'Zamora', 'Zaragoza'];

export const statusOptions: string[] = ['ongoing', 'idea'];

