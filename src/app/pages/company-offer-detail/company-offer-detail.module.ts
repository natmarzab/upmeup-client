import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonModal, IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { CompanyOfferDetailPageRoutingModule } from './company-offer-detail-routing.module';
import { CompanyOfferDetailPage } from './company-offer-detail.page';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CompanyOfferDetailPageRoutingModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  declarations: [CompanyOfferDetailPage],
  providers: [MultilanguageComponent, IonModal]
})
export class CompanyOfferDetailPageModule {}
