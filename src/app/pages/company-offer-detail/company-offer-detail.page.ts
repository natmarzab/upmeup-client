/* eslint-disable no-underscore-dangle */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Apollo, gql } from 'apollo-angular';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import { IonModal, ModalController } from '@ionic/angular';
import { CandidatureStatus } from 'src/app/models/candidature-status.model';
import { User } from 'src/app/models/user';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { UserService } from 'src/app/services/user.service';
import { Offer } from '../../models/offer';
import { EditOfferPage } from '../edit-offer/edit-offer.page';
import { OfferStatus } from 'src/app/models/offer-status.model';

const GET_USOFRS = gql`
  query {
    getUsersOffers {
      _id
      offer_id
      user_id
    }
  }
`;

@Component({
  selector: 'app-company-offer-detail',
  templateUrl: './company-offer-detail.page.html',
  styleUrls: ['./company-offer-detail.page.scss'],
})
export class CompanyOfferDetailPage implements OnInit, OnDestroy {
  offerID: any;
  offer: Offer;
  userOfferList: any[] = [];
  usersList: any[] = [];
  usersListData: any[] = [];
  uDataList: any[] = [];
  enrolledUser: any[] = [];
  uSkills: any[] = [];
  oSkill: any[] = [];
  userLoggedSkills: any[] = [];
  sortUsersList: any[] = [];
  userInfo: User;
  avatar: string;
  showOption: any = 'false';
  openProcess: any = 'false';

  readonly candidatureStatus = CandidatureStatus;
  readonly offerStatus = OfferStatus;

  error: any;
  loading = true;
  private querySubscription: Subscription;

  constructor(
    private aRoute: ActivatedRoute,
    private apollo: Apollo,
    private mController: ModalController,
    private compOfService: CompanyOffersService,
    private uService: UserService,
    public router: Router,
    public modal: IonModal) { }

  ngOnInit() {
    this.offerID = this.aRoute.snapshot.params.id;
    this.qOfferById(this.offerID);
    this.getLoggedUser();
  }
  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
      this.userLoggedSkills = item.me.softSkill;
    });
  }

  // Call Query to Get info of selected offer (by ID).
  qOfferById(ofID: any) {
    this.compOfService.qGetOffer(ofID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      if (!item) {
        console.log('Ops, sembla que no hi han dades que mostrar....');
      } else {
        this.offer = item.getOffer;
        this.usersListData = this.offer.candidates.map(offer => ({ ...offer, match: { ...offer.match, total: Math.round(offer.match.total) } }));
        this.usersListData.sort((a) => (a.status === "REJECTED" ) ? 1 : -1)

        
      }
    });
  }


  preselectCandidate(candidateId: String) {
    this.updateCandidateStatus(candidateId, CandidatureStatus.PRESELECTED);
  }

  rejectCandidate(candidateId: String) {
    this.updateCandidateStatus(candidateId, CandidatureStatus.REJECTED);
  }

  undefineCandidate(candidateId: String) {
    this.updateCandidateStatus(candidateId, CandidatureStatus.UNDEFINED);
  }

  updateCandidateStatus(candidateId: String, candidatureStatus: CandidatureStatus) {
   this.compOfService.mUpdateCandidateStatus(this.offerID, candidateId, candidatureStatus).subscribe();
  }

  
  updateHiring(candidateId: String){
    for (let candidate of this.offer.candidates){
      if (candidateId === candidate.user._id){
        if (candidate.status === "PRESELECTED"){
          this.updateCandidateStatus(candidateId, CandidatureStatus.HIRED);
        }
        else if (candidate.status === "HIRED"){
          this.updateCandidateStatus(candidateId, CandidatureStatus.PRESELECTED);
        }
      }
    }
  }


  /**
   * Call modal to edit offer values
   */
  async editModal() {

    const editModal = await this.mController.create({
      component: EditOfferPage,
      componentProps: {
        offerData: this.offer
      },
      animated: true,
      cssClass: 'modalCss'
    });

    await editModal.present();

  }

  showOptions(x){
      this.showOption = x;
  }

  disableOffer(offerId: String) {
      this.updateOfferStatus(offerId, OfferStatus.INACTIVE);
  }

  enableOffer(offerId: String) {
    this.updateOfferStatus(offerId, OfferStatus.ACTIVE);
}

  updateOfferStatus(OfferId: String, offerStatus: OfferStatus) {
    this.compOfService.mUpdateOfferStatus(this.offerID, offerStatus).subscribe();
   }

  deleteOffer(ofId){
    this.compOfService.mDeleteOffer(ofId)
    .subscribe(() => {
      console.log('Offer deleted!');
    });
    this.mController.dismiss()
    this.router.navigate(["company-offer"])
  }

  cancel() {
    this.mController.dismiss()
  }

  confirm() {
    this.modal.dismiss();
  }

  goCandidateProfile(userid){
    this.router.navigate(["interested-entity"], {state: {userid: userid }})
  }

  ngOnDestroy() {
    /* this.querySubscription.unsubscribe();*/
  }

  createChat(adminID){
    this.uService.mCreateChatRoomIfNotExists(this.userInfo._id, adminID).subscribe(() => {
      this.qGetLoggedUser(this.userInfo._id, adminID);
      
    }); 
     
  }

  qGetLoggedUser(userID, adminID){
    this.uService.qGetUser(userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
        this.userInfo = item.getUser;
        this.getChatUrl(adminID);
    })
  }

  getChatUrl(adminID) {
    this.router.navigate([`/chat/${this.userInfo?.rooms?.find(room => room.userId === adminID)?.roomId}`])
  }

}
