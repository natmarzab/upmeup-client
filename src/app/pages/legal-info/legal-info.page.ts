import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/services/auth.service";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user.service";
import { map } from "rxjs";


@Component({
    selector: 'app-legal-info',
    templateUrl: './legal-info.page.html',
    styleUrls: ['./legal-info.page.scss'],
  })
  export class LegalInfoPage implements OnInit {
    userInfo: User;
    userType = '0';

    constructor(
      public auth: AuthService,
      private uService: UserService
      
      ){}


  ngOnInit(): void {
        this.getLoggedUser();
  }
  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type;
    });
  }

  }