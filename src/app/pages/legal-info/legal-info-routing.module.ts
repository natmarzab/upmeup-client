import { RouterModule, Routes } from "@angular/router";
import { LegalInfoPage } from "./legal-info.page";
import { NgModule } from "@angular/core";

const routes: Routes = [
    {
      path: '',
      component: LegalInfoPage
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class LegalInfoPageRoutingModule { }