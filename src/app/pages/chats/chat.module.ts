import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { MenuMobileModule } from "src/app/shared/menu-mobile/menu-mobile.module";
import { ToolbarModule } from "src/app/shared/toolbar/toolbar.module";
import { ChatPage } from "./chat.page";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { HttpLoaderFactory } from "src/app/app.module";
import { HttpClient } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { ChatPageRoutingModule } from "./chat-routing.module";


@NgModule({
    imports: [
        ToolbarModule,
        MenuMobileModule,
        IonicModule,
        CommonModule,
        ChatPageRoutingModule,
        TranslateModule.forChild({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        })
    ],
    declarations: [ChatPage]
})
export class ChatPageModule {}