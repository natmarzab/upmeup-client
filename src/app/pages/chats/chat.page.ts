import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import { firstValueFrom } from "rxjs";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user.service";
import { environment } from "src/environments/environment";


@Component({
    selector: 'app-chat',
    templateUrl: './chat.page.html',
    styleUrls: ['./chat.page.scss']
})
export class ChatPage implements OnInit, AfterViewInit {

    private chatId: string;
    private me: User;

    @ViewChild('rocketchat', { static: false, read: ElementRef })
    private rocketChat: ElementRef<HTMLIFrameElement>;

    constructor(private aRoute: ActivatedRoute, private sanitizer: DomSanitizer, private uService: UserService) { }
    ngAfterViewInit(): void {
        this.rocketChat.nativeElement.src=`${environment.ROCKETCHAT_URL}?layout=embedded`
        this.getMe();
    }

    ngOnInit(): void {
        this.chatId = this.aRoute.snapshot.params.chatId;
    }

    async getMe() {
        
        this.me = (await firstValueFrom(this.uService.qGetChatToken().valueChanges)).data.getChatToken;
        const authToken = this.me.chatAuthToken;
        const rocket = this.rocketChat;
        const chatId = this.chatId
        window.addEventListener('message', function (e) {
            if(e.data.eventName === 'startup') {
                rocket.nativeElement.contentWindow.postMessage({
                    externalCommand: 'login-with-token',
                    token: authToken
                }, '*');
                rocket.nativeElement.contentWindow.postMessage({
                    externalCommand: 'go',
                    path: `/direct/${chatId}`
                }, '*');
            }
        });
    }
}