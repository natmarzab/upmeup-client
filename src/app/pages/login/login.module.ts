import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NgxPwaInstallModule } from 'ngx-pwa-install';
import { LoginPageRoutingModule } from './login-routing.module';
import { LoginPage } from './login.page';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    LoginPageRoutingModule,
    NgxPwaInstallModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [LoginPage],
})

export class LoginPageModule {}
