/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, MenuController } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  userData: any = '';
  error: any = '';
  initForm: FormGroup;
  isSubmitted = false;
  emailRegex = '^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,10}$'; 

  user = { email: '', pw: '' };
  userType = '0';
  userExist = false;
  roleMsg = '';
  dismissed = false;

  constructor(private menu: MenuController,
    public fBuilder: FormBuilder,
    private router: Router,
    private auth: AuthService,
    private userService: UserService,
    private appC: AppComponent,
    public loadingCtrl: LoadingController) { }

  ngOnInit() {
    if(this.auth.isLogged()) {
      this.doLogin();
    }
    this.menu.enable(false);
    this.validation();
    this.initForm.reset();
  }

  validation() {
    this.initForm = this.fBuilder.group({
      userEmail: ['', [Validators.required, Validators.pattern(this.emailRegex)]],
      userPassword: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  submitForm() {
    this.userType = '0';
    this.userExist = false;

    if (this.initForm.valid) {
      this.user.email = this.initForm.value.userEmail.toLowerCase();
      this.user.pw = this.initForm.value.userPassword;
      this.loginIn(this.user.email, this.user.pw);
    }

    return false;
  }


  // Check if user exist
  loginIn(uEmail, uPassword) {
    this.userService.qGetJWT(uEmail, uPassword).subscribe(({ data: { getJWTByEmailAndPassword: { token } } }) => {
      if (token.length > 0) {
        localStorage.setItem('token', token);
        this.doLogin()
      } else { this.isSubmitted = true }
    });
  }

  doLogin() {
    this.auth.onLogin();
    this.userService.qGetMe().valueChanges.subscribe(response => {
      const loggedUser = response.data.me;
      this.userExist = true;
      this.useSessionStorage(loggedUser._id, loggedUser.name, loggedUser.email, loggedUser.type, loggedUser.softSkills.map(s => s.name));
      this.userType = loggedUser.type;

      if (window.sessionStorage) {
        sessionStorage.removeItem('uSelectedSkills');
      }

          if (this.userType === '1') {
            this.router.navigate(['/user-profile'], {state: {button: '1'}});
          } else if (this.userType === '2') {
            this.router.navigate(['/company-profile'], {state: {button: '1'}});
          }

          sessionStorage.setItem("menuMobile", "true");

      this.menu.enable(true);
      this.isSubmitted = true;
      return;
    })
  }

  get errorControl() {
    return this.initForm.controls;
  }

  useSessionStorage(uid, uName, uMail, uType, uSkills) {
    sessionStorage.setItem('userid', uid);
    sessionStorage.setItem('user', uName);
    sessionStorage.setItem('email', uMail);
    sessionStorage.setItem('type', uType);
    sessionStorage.setItem('uSelectedSkills', uSkills);
  }

  alertMail() {
    alert('Ponte en contacto con: lmata@facto.cat');
  }

  showPass(){
    const password = document.querySelector("#userPassword");
    const type = password.getAttribute("type") === "password" ? "text" : "password";
    password.setAttribute("type", type);
  }

}
