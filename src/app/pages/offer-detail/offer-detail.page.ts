/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';

import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { Candidate, Offer } from '../../models/offer';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';

@Component({
  selector: 'app-offer-detail',
  templateUrl: './offer-detail.page.html',
  styleUrls: ['./offer-detail.page.scss'],
})
export class OfferDetailPage implements OnInit {
  userLoggedID = sessionStorage.getItem('userid');
  offer: Offer;
  offerID: any;
  userID: any;
  enrolledValue = '';
  userInfo: User;
  userSkills: any[] = [];
  skillsName: any;
  skillsIco: any[] = [];
  usersOffersList: any[] = [];
  userCompets: any[] = [];
  allOffers: any[] = [];
  match: any;
  files: any[];
  candidature: Candidate;
  roomId: string;

  constructor(
    private aRoute: ActivatedRoute,
    private router: Router,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private translateList: MultilanguageComponent,
    private cOfferService: CompanyOffersService,
    private uService: UserService) {}

  ngOnInit() {
    this.offerID = this.aRoute.snapshot.params.id;
    this.qGetOffer(this.offerID);
    
  }

  /**
   * Query to get Offer details
   * @param ofId
   */
  qGetOffer(ofId: string) {
    this.cOfferService.qGetOffer(ofId).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      if (!item) {
        console.log('Ops, sembla que no hi han dades que mostrar....');
      } else {
        this.offer = {
          ...item.getOffer,
          match: Math.round(item.getOffer.match),
          user: {
            ...item.getOffer.user,
            softSkills: item.getOffer.user.softSkills
              .map(s => ({ class: s.name.replace(/ /g, '_').normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase(), ...s }))
          }
        };
        this.roomId = this.offer.user.rooms.find(room => room.userId == this.userLoggedID)?.roomId;
        this.candidature = this.offer.candidates.find(candidate => candidate.user._id === this.userLoggedID)
        this.files = this.offer.file
      }
    });
  }
  

  /**
   * Action of button "Inscribir".
   */
  addEnrroled(ofID, ofTitle) {
    this.cOfferService.mAddCandidate(ofID, this.userLoggedID).subscribe(() => {
      // Message Alert!
      this.enrolled();
      this.goToList();
    });

    // this.uService.mAddCandidature(this.userLoggedID, ofID, ofTitle).subscribe(() => {
    //   console.log('Candidatura añadida correctamente!');
    // });
  }

  goToList() {
    this.router.navigate(['/candidatures']);
  }

  // Alert to confirm an action
  async enrolled() {
   // this.loadingCtrl.dismiss();
    this.translateList.translate.stream('toast-offer-enrolled').subscribe(async (value) => {
      const toast = await this.toastCtrl.create({
        color: 'dark',
        message: value,
        duration: 3000,
        position: 'middle'
      });
      toast.present();
    });  
  }

  removeCandidate(ofID, ofTitle) {
    console.log('borrando usuaria')
    this.cOfferService.mRemoveCandidate(ofID, this.userLoggedID).subscribe(() => {
      this.removed();  
    });  
  }
  async removed() { 
     this.translateList.translate.stream('toast-candidate-removed').subscribe(async (value) => {
             const toast = await this.toastCtrl.create({
               color: 'dark',
               message: value,
               duration: 3000,
               position: 'bottom'
             });
             toast.present();
           });  
   }



  goToWebsite(){
    // console.log(`Current userInfo: ${JSON.stringify(this.offer)}`)
    const website = this.offer.user.website
    
    const url = website.substring(0,4) === 'http' // http / https
        ? this.offer.user.website
        : `https://`+ this.offer.user.website // default to https
    // console.log(`goToLink; ${url}`)
    window.open(url, "_blank");
  }

  async onDownloadFile(offerID: string, file: string) {
    this.cOfferService.qGetDownloadFile(offerID, file)
      .subscribe(response => {
        window.open(response.data.downloadFile.presignedUrl);
      });
  }

  getChatUrl() {
    return `/chat/${this.roomId}`;
  }

}

