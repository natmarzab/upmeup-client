import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { TagInputModule } from "ngx-chips";
import { MenuMobileModule } from "src/app/shared/menu-mobile/menu-mobile.module";
import { MultilanguageModule } from "src/app/shared/multilanguage/multilanguage.module";
import { ToolbarModule } from "src/app/shared/toolbar/toolbar.module";
import { DeleteAccountPageRoutingModule } from "./delete-account-routing.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { HttpLoaderFactory } from "src/app/app.module";
import { DeleteAccountPage } from "./delete-account.page";
import { MultilanguageComponent } from "src/app/shared/multilanguage/multilanguage.component";

@NgModule({
    imports: [
      MultilanguageModule,
      ToolbarModule,
      MenuMobileModule,
      CommonModule,
      TagInputModule,
      IonicModule,
      DeleteAccountPageRoutingModule,
      //ngx-translate and loader module
      HttpClientModule,
      TranslateModule.forChild({
          loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
          }
      })
    ],
    declarations: [DeleteAccountPage], //, ProgressBarComponent]
    providers: [MultilanguageComponent]
  })
  export class DeleteAccountPageModule {}