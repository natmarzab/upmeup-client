/* eslint-disable no-underscore-dangle */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-candidatures',
  templateUrl: './candidatures.page.html',
  styleUrls: ['./candidatures.page.scss'],
})
export class CandidaturesPage implements OnInit {
  allUsersOffers: any[] = [];
  usersOffersList: any[] = [];
  enrolledOffers: any[] = [];
  avatarB64: string;
  userType: string;
  avatar: string;
  userInfo: User;
  userID: string;

  constructor(
               private router: Router,
               private uService: UserService,
               private offServ: CompanyOffersService) {}

  ngOnInit() {
    this.userID = sessionStorage.getItem('userid')
    this.getCandidatures();
    this.getLoggedUser();
  }


  getCandidatures() {
    this.offServ.qGetAllOffers().valueChanges.pipe(
      map(result => result.data.getCompanyOffers)
    ).subscribe((candidatures) => {
      this.usersOffersList = candidatures.filter((c) => (JSON.stringify(c.candidates).includes(this.userID) === true ));

      for(let offer of this.usersOffersList){
        this.avatarB64 = offer.user.avatarB64
      }
    })
    
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type
      this.avatar = item.me.avatarB64;
    });
  }

  // Get offers detail
  goOfferDetail(offer) {
    const id = offer._id;
    this.router.navigate(['/offer-detail', id]);
  }

}
