import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { Mp1Component } from './mp1';
import { Mp1ComponentRoutingModule } from './mp1-routing.module';
@NgModule({
  imports: [
      MultilanguageModule,
      CommonModule,
      TagInputModule,
      FormsModule,
      ReactiveFormsModule,
      IonicModule,
      Mp1ComponentRoutingModule,
      //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [Mp1Component],
  providers: [MultilanguageComponent]
})
export class Mp1ComponentModule {}