import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {
  userInfo: User;
  userType= '0';
  userData: any = '';
  error: any = '';
  initForm: FormGroup;
  isSubmitted = false;
  emailRegex = '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/';
  avatar: string;


  user = { email: '', pw: '' };
  userExist = false;

  constructor(private router: Router,
              private uService: UserService,
              public fBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.getUser()
  }

  getUser(){
    this.uService.qGetMe().valueChanges.subscribe(response => {
      const loggedUser = response.data.me;
      this.userExist = true;
      this.useSessionStorage(loggedUser._id, loggedUser.name, loggedUser.email, loggedUser.type, loggedUser.softSkills.map(s => s.name));
      this.userType = loggedUser.type;
      this.userInfo = loggedUser;
      this.avatar = this.userInfo.avatarB64;
    
    });
      
  }




   async goToProfile() {
    
      if (window.sessionStorage) {
        sessionStorage.removeItem('uSelectedSkills');
      }

      if (this.userType === '1') {
        this.router.navigate(['/user-profile'], {state: {button: '1'}});
      } else if (this.userType === '2') {
        this.router.navigate(['/company-profile'], {state: {button: '1'}});
      }
  }

  goToCreateOffer(){

      if (window.sessionStorage) {
        sessionStorage.removeItem('uSelectedSkills');
      }
    
      this.router.navigate(['/company-offer'], {state: {button: '2'}});
  
  }


  goToOfferList(){
   
      if (window.sessionStorage) {
        sessionStorage.removeItem('uSelectedSkills');
      }
      
      this.router.navigate(['/offer-list'], {state: {button: '3'}});

  }

  useSessionStorage(uid, uName, uMail, uType, uSkills) {
    sessionStorage.setItem('userid', uid);
    sessionStorage.setItem('user', uName);
    sessionStorage.setItem('email', uMail);
    sessionStorage.setItem('type', uType);
    sessionStorage.setItem('uSelectedSkills', uSkills);
  }

}
