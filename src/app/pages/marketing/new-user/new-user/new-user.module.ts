import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { NewUserComponentRoutingModule } from './new-user-routing.module';
import { NewUserComponent } from './new-user.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';
@NgModule({
  imports: [
      MultilanguageModule,
      ToolbarModule,
      MenuMobileModule,  
      CommonModule,
      TagInputModule,
      FormsModule,
      ReactiveFormsModule,
      IonicModule,
      NewUserComponentRoutingModule,
      //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [NewUserComponent],
  providers: [MultilanguageComponent]
})
export class NewUserComponentModule {}
