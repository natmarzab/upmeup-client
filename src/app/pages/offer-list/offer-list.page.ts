/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable no-underscore-dangle */
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { InfiniteScrollCustomEvent, IonInfiniteScroll } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { User } from 'src/app/models/user';

import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList, contractTypeOptions, remoteOptions, workHourOptions } from 'src/app/utils/constants';

const PAGE_SIZE = 10;

@Component({ 
  selector: 'app-offer-list',
  templateUrl: './offer-list.page.html',
  styleUrls: ['./offer-list.page.scss'],
})
export class OfferListPage implements OnInit {
  allOffers: any[] = [];
  activeOffers: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  currPage: number = 0;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  readonly cityList = cityList;
  readonly workHourOptions = workHourOptions;
  readonly contractTypeOptions = contractTypeOptions;
  readonly remoteOptions = remoteOptions;

  readonly RequiredExperience = RequiredExperience;
  userType: string;
  avatar: string;
  userInfo: User;
  candidatures: any[];

  userLogged = {
    userID: sessionStorage.getItem('userid'),
    values: JSON.parse(sessionStorage.getItem('uSelectedSkills'))
  };

  public filters: FormGroup;

  constructor(private router: Router,
    public fBuilder: FormBuilder,
    private compOfService: CompanyOffersService, 
    private uService: UserService,
    private translateList: MultilanguageComponent) {
    }

  ngOnInit() {
    this.getLoggedUser();
    this.qGetOffers();    
    this.filters = this.fBuilder.group({
      search: [undefined],
      createdDate: [undefined],
      city: [undefined],
      contractType: [undefined],
      workHours: [undefined],
      remote: [undefined],
      requiredExperience: [undefined]
    });
    this.finalCityList = this.translateList.translateCityList();

    if(!this.router.getCurrentNavigation().extras.state){   
    }
    else{
            this.router.navigate(['/mp1'], {state: {button: '3'}});
    }
  }

  /**
   * Get offers from DB.
   */
  qGetOffers(filters?: any) {
    this.compOfService.qGetAllOffers(filters, PAGE_SIZE, PAGE_SIZE*this.currPage).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.allOffers = item.getCompanyOffers
        .filter(offer => (offer.status === 'ACTIVE' || offer.status === null))
        .map(offer => ({...offer, match: Math.round(offer.match), enrolled: offer.candidates && offer.candidates.findIndex(candidate => candidate.user._id === this.userInfo._id) !== -1}))
        .sort((a, b) => (a.match > b.match) ? -1 : 1);
        
    }); 
  } 



  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type
      this.avatar = item.me.avatarB64;
      this.candidatures = item.me.candidatures;
    });    
  }

  doInfinite(e) {
    this.currPage++;
    this.compOfService.qGetAllOffers(this.formatFilters(), PAGE_SIZE, PAGE_SIZE*this.currPage).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      if(item.getCompanyOffers.length < PAGE_SIZE) {
        (e as InfiniteScrollCustomEvent).target.disabled = true; //When we retrieve the last offers we disable the infinite scroll
      }

      this.allOffers = [...this.allOffers, ...item.getCompanyOffers
        .map(offer => ({...offer, match: Math.round(offer.match), enrolled: offer.candidates && offer.candidates.findIndex(candidate => candidate.user._id === this.userInfo._id) !== -1}))
        .sort((a, b) => (a.match > b.match) ? -1 : 1)];
      e.target.complete();  
    });
  }

  // Go to offer detail
  goOfferDetail(offer) {
    const id = offer._id;
    this.router.navigate(['/offer-detail', id]);
  }

  // Go to candidatures
  toCandidatures() {
    this.router.navigate(['/candidatures']);
  }

  formatFilters() {
    const filters: any = {}

    if (this.filters.value.search)
      filters.search = this.filters.value.search
    if (this.filters.value.city)
      filters.city = this.filters.value.city
    if (this.filters.value.createdDate) {
      filters.createdDate = new Date();
      filters.createdDate.setHours(0,0,0,0)
      switch(this.filters.value.createdDate) {
        case 'last-24':
          filters.createdDate.setDate(filters.createdDate.getDate() - 1);
          break;
        case 'last-7':
          filters.createdDate.setDate(filters.createdDate.getDate() - 7);
          break;
        case 'last-15':
          filters.createdDate.setDate(filters.createdDate.getDate() - 15);
          break;
      }
    }
    if (this.filters.value.workHours)
      filters.workHours = this.filters.value.workHours
    if (this.filters.value.contractType)
      filters.contractType = this.filters.value.contractType
    if (this.filters.value.remote)
      filters.remote = this.filters.value.remote
    if (this.filters.value.requiredExperience)
      filters.requiredExperience = this.filters.value.requiredExperience

    if(Object.keys(filters).length==0)
      return undefined;

    return filters 
  }

  onFilter(){
    this.currPage = 0;
    this.infiniteScroll.disabled = false;
    this.qGetOffers(this.formatFilters());
  }

  clearFilters() {
    this.currPage = 0;
    this.infiniteScroll.disabled = false;
    this.filters.reset();
    this.qGetOffers();
  }

}
