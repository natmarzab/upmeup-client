import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OfferListPageRoutingModule } from './offer-list-routing.module';

import { OfferListPage } from './offer-list.page';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    OfferListPageRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [OfferListPage],
  providers: [MultilanguageComponent]
})
export class OfferListPageModule {}
