/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { firstValueFrom, map } from 'rxjs';
import { Offer } from 'src/app/models/offer';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { CompetenceService } from 'src/app/services/competence.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList } from '../../utils/constants';
import { User } from 'src/app/models/user';
import { OfferStatus } from 'src/app/models/offer-status.model';
import { Router } from '@angular/router';


@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.page.html',
  styleUrls: ['./edit-offer.page.scss'],
})
export class EditOfferPage implements OnInit {
  readonly cityList = cityList

  readonly RequiredExperience = RequiredExperience;

  editForm: FormGroup;
  isSubmitted = false;
  offerID: any;
  competList: any[] = [];
  avatar: string;

  offerCompetIDs: any[] = [];
  selectedCompet: any[] = [];
  newOfferCompets: any[] = [];
  newOfferCompetsList: any[] = [];
  nameNewCompet: any[] = [];
  selectedLang = "";
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  userInfo: User;
  file: any;
  files: any[] = [];
  filenames: any[] = [];
  deletedFile: any;
  status: any;
  showOption: any = 'false';
  newCompet: any;

  @Input() offerData: Offer;

  constructor(
    public fBuilder: FormBuilder,
    private mdlController: ModalController,
    private compOfService: CompanyOffersService,
    private competService: CompetenceService,
    public loadingCtrl: LoadingController,
    private translateList: MultilanguageComponent,
    private uService: UserService,
    private toastCtrl: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
    this.getAvatar();
    this.initForm();
    this.offerCompetIDs = this.offerData.competencies.map(c => c._id);
    this.selectedCompet = this.offerData.competencies;
    this.qGetCompetencies();
    this.setValues(this.offerData);
    this.finalCityList = this.translateList.translateCityList();
    this.getLoggedUser();
    this.getStatus();

    for (let file of this.offerData.file) {
      this.filenames.push(file);
    }
  }

  getAvatar() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.avatar = item.me.avatarB64;
    });
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
    });
  }

  getStatus() {
    this.compOfService.qGetOffer(this.offerData._id).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.status = item.getOffer.status;
    })
  }

  // Get competence List
  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.competList = item.getCompetencies;
    });
  }

  // Initialized form
  initForm() {
    this.editForm = this.fBuilder.group({
      iTitle: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100)]],
      iEduLevel: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      iCompetence: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      iCity: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      iRangoSalarial: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(60)]],
      iRequiredExperience: ['', [Validators.required]],
      iRemoto: ['', [Validators.required]],
      iTipoContrato: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(35)]],
      iJornada: ['', [Validators.required, Validators.maxLength(30)]],
      iDescripcio: ['', [Validators.required, Validators.minLength(40), Validators.maxLength(1000)]],
      iRequirements: ['', [Validators.required, Validators.minLength(40), Validators.maxLength(1000)]]
    });

  }

  // Set values form component.
  setValues(infoOffer) {
    this.offerID = infoOffer._id;
    this.editForm.get('iTitle').setValue(infoOffer.title);
    this.editForm.get('iEduLevel').setValue(infoOffer.eduLevel);
    this.editForm.get('iCompetence').setValue(infoOffer.competencies);
    this.editForm.get('iCity').setValue(infoOffer.city);
    this.editForm.get('iRangoSalarial').setValue(infoOffer.salaryRange);
    this.editForm.get('iRemoto').setValue(infoOffer.remote);
    this.editForm.get('iTipoContrato').setValue(infoOffer.contractType);
    this.editForm.get('iJornada').setValue(infoOffer.workHours);
    this.editForm.get('iDescripcio').setValue(infoOffer.description);
    this.editForm.get('iRequiredExperience').setValue(infoOffer.requiredExperience);
    this.editForm.get('iRequirements').setValue(infoOffer.requirements);
  }

  // Submit Form
  async onSubmit() {
    this.isSubmitted = true;
    if (!this.editForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      await this.insertedTags(this.editForm.value.iCompetence);
      for (let file of this.filenames) {
        if (file.split('.').pop() === 'pdf') {
        }
        else {
          this.loadingCtrl.dismiss();
          this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'dark',
              message: value,
              duration: 3000,
              position: 'middle'
            });
            await toast.present();

          });
          return;
        }
      }

      this.loadingCtrl.create({
        message: 'Editando oferta...'
      }).then(async res => {
        res.present();
        console.log('Guardando datos...');
        await this.editOffer(
          this.offerID,
          this.editForm.value.iTitle,
          this.editForm.value.iEduLevel,
          this.editForm.value.iCity,
          this.editForm.value.iJornada,
          this.newOfferCompetsList,
          this.editForm.value.iRangoSalarial,
          this.editForm.value.iRemoto,
          this.editForm.value.iTipoContrato,
          this.editForm.value.iDescripcio,
          this.editForm.value.iRequirements,
          this.editForm.value.iRequiredExperience,
          this.filenames
        );
        this.loadingCtrl.dismiss();
      });
    }
  }

  // Created new competence if not exist yet
  async insertedTags(competencies) {
    for (let i = 0; i < competencies.length; i++) {
      if (competencies[i]._id) {
        this.newOfferCompetsList.push({ "_id": competencies[i]._id, "name": competencies[i].name })
      } else {
        const competency = this.competList.find(c => c.name.trim().toLowerCase() === competencies[i].name.trim().toLowerCase())
        if (!competency) {
          const newCompetency = await this.createNewCompetence(competencies[i].name);
          this.newOfferCompetsList.push({ "_id": newCompetency._id, "name": newCompetency.name });
        } else {
          this.newOfferCompetsList.push({ "_id": competency._id, "name": competency.name });
        }
      }
    }
  }

  // Create new competence:
  async createNewCompetence(iName: any) {
    return firstValueFrom(this.competService.mCreateCompetence(iName));
  }

  /**
   * Get values from form and update info.
   */
  editOffer(oId: any, iTitle: any, iEduLevel: any, iCity: any, iJornada: any, iRango: any, iRemoto: any, iContrato: any, iDescripcio: any, iRequirements: any, iCompetence: any, iRequiredExperience: RequiredExperience, iFile: any) {
    this.compOfService.mEditOffer(oId, iTitle, iEduLevel, iCity, iJornada, iRango, iRemoto, iContrato, iDescripcio, iRequirements, iCompetence, iRequiredExperience, iFile)
      .subscribe((response) => {
        console.log('Oferta editada!');
        for (let file of this.files) {
          this.compOfService.qGetUploadOfferUrl(oId, file.name).subscribe(response => {
            const formData = new FormData();
            formData.append('file', file)
            fetch(response.data.uploadOfferUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
              if (response.ok) {
                console.log('Archivo con nombre', file.name, 'subido correctamente!')
              }
            });
          });
        }
      });
    this.dismissEditModal();
  }

  get errorCtr() {
    return this.editForm.controls;
  }

  matchCompetencies = (query: string, item: any): boolean => {
    return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
  }

  // Delete function to competencies/tags.
  removeCompetence(item) {
    const tags = this.editForm.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  onSelectFile(event): any {
    this.filenames.push(event.target.files[0].name)
    this.files.push(event.target.files[0]);
  }

  onDeleteFile(file): any {
    const index = this.filenames.indexOf(file);
    this.filenames.splice(index, 1);
  }

  showOptions(x) {
    if (x === this.showOption) {
      this.showOption = 4
    }
    else (this.showOption = x)
  }

  disableOffer(oId: String) {
    this.updateOfferStatus(oId, OfferStatus.INACTIVE);
  }

  enableOffer(oId: String) {
    this.updateOfferStatus(oId, OfferStatus.ACTIVE);
  }

  updateOfferStatus(oId: String, offerStatus: OfferStatus) {
    this.compOfService.mUpdateOfferStatus(oId, offerStatus).subscribe();
  }

  deleteOffer(oId) {
    this.compOfService.mDeleteOffer(oId)
      .subscribe(() => {
        this.deleted();
        console.log('Offer deleted!');
        // this.mService.mDeleteProjectMatches(pId).subscribe(() => {
        // })
        this.dismissModal();
        this.router.navigate(["company-offer"]);
        return;
      });

  }

  async deleted() {
    const toast = await this.toastCtrl.create({
      color: 'dark',
      message: 'Oferta eliminada correctamente!',
      duration: 1500,
      position: 'middle'
    });

    await toast.present();
    this.dismissModal();
  }

  /**
   * Close modal when update
   */
  async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }

}

