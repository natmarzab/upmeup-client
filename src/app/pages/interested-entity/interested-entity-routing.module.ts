import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterestedEntityComponent } from './interested-entity.component';

const routes: Routes = [
  {
    path: '',
    component: InterestedEntityComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterestedEntityComponentRoutingModule { }