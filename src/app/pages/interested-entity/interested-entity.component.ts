import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { firstValueFrom, map } from "rxjs";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user.service";


@Component({
    selector: 'app-interested-entity',
    templateUrl: './interested-entity.component.html',
    styleUrls: ['./interested-entity.component.scss']
})
export class InterestedEntityComponent implements OnInit {
    
    userID: string;
    skillsList: any[] = [];
    userInfo: User;
    userSkills: any[] = [];
    skillsName: any;
    skillsIco: any[] = [];
    compet: any;
    userCompets: any[] = [];
    sectorName = '';
    avatar: string; 
    userType: any;
    me: User;
    
    constructor(private router: Router,
                private uService: UserService){}

    ngOnInit(){
      if(this.router.getCurrentNavigation().extras?.state != null){ 
          sessionStorage.setItem('interestedID', this.router.getCurrentNavigation().extras.state.userid)
        }
      this.userID = sessionStorage.getItem('interestedID')
      this.getMe();
      this.getUser();
    }

    getMe() {
      this.uService.qGetMe().valueChanges.subscribe(item => {
        this.me = item.data.me;
      })
    }

    getUser() {
        this.uService.qGetUser(this.userID).valueChanges.pipe(
          map(result => result.data)
        ).subscribe((item) => {
          this.userInfo = item.getUser;
          this.avatar = item.getUser.avatarB64;
          this.setSoftSkills(item.getUser.softSkills);
          this.userCompets = item.getUser.competencies;
          this.sectorName = item.getUser.sector.name;
          this.userType = item.getUser.type;
        });
    }

    setSoftSkills(softSkills) {
        this.userSkills = softSkills;
        this.useSessionStorage(this.userSkills);
        this.skillsIco = [];
        softSkills.forEach(skill => {
          const nameClass = skill.name.replace(/ /g, '_');
          this.skillsName = nameClass.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
          this.skillsIco.push({
            sid: skill._id,
            sname: skill.name,
            sClass: this.skillsName
          });
        })
    }

    useSessionStorage(skills) {
    sessionStorage.setItem('uSelectedSkills', JSON.stringify(skills));
    }

    async downloadCV() {
      this.uService
        .qGetDownloadCVUrl(this.userInfo._id)
        .subscribe(response => {
          window.open(response.data.downloadCvUrl.presignedUrl);
        });
    }

    goToWebsite(){
      const website = this.userInfo.website
      
      const url = website.substring(0,4) === 'http'
          ? this.userInfo.website
          : `https://`+ this.userInfo.website 
      window.open(url, "_blank");
    }

    chatAvailable() {
      return this.userInfo?.rooms?.find(room => room.userId === this.me?._id)?.roomId !== undefined;
    }

    getChatUrl() {
      return `/chat/${this.userInfo?.rooms?.find(room => room.userId === this.me?._id)?.roomId}`;
    }
}