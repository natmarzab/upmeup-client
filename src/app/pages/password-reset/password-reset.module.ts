import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NgxPwaInstallModule } from 'ngx-pwa-install';
import { PasswordReset } from './password-reset.page';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { PasswordResetPageRoutingModule } from './password-reset-routing.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PasswordResetPageRoutingModule,
    IonicModule,
    NgxPwaInstallModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  providers: [MultilanguageComponent],
  declarations: [PasswordReset],
})

export class PasswordResetModule {}
