/* eslint-disable max-len */
/* eslint-disable no-underscore-dangle */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { firstValueFrom, map } from 'rxjs';
import { RequiredExperience } from 'src/app/models/required-experience.model';
import { CompanyOffersService } from 'src/app/services/company-offers.service';
import { CompetenceService } from 'src/app/services/competence.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList } from '../../utils/constants';
import { User } from 'src/app/models/user';
import { OfferStatus } from 'src/app/models/offer-status.model';

@Component({
  selector: 'app-create-offer',
  templateUrl: './create-offer.page.html',
  styleUrls: ['./create-offer.page.scss'],
})
export class CreateOfferPage implements OnInit {
  readonly cityList = cityList
  readonly RequiredExperience = RequiredExperience;

  createForm: FormGroup;
  isSubmitted = false;
  userID: any;
  cDate: string;
  addEnroll = 0;

  competList: any[] = [];
  offerCompets: any[] = [];
  offerCompetsIds: any[] = [];
  newOfferCompetsList: any[] = [];
  nameNewCompet: any = [];
  selectedLang= "";
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  avatar: string; 
  userInfo: User;
  offerStatus: OfferStatus;
  filenames: any[] = [];
  files: any[] = [];
  offerInfo: any;
  offerID: string;

  @Input() mode: String;

  constructor(
              public fBuilder: FormBuilder,
              private mdlController: ModalController,
              private alrtController: AlertController,
              private compOfService: CompanyOffersService,
              private competService: CompetenceService,
              public loadingCtrl: LoadingController,
              private translateList: MultilanguageComponent,
              private uService: UserService,
              private toastCtrl: ToastController) {}

  ngOnInit() {
    this.userID = sessionStorage.getItem('userid');
    this.getAvatar();
    this.qGetCompetencies();
    this.validation();
    this.finalCityList = this.translateList.translateCityList();
    this.getLoggedUser();
  }

  getAvatar(){
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.avatar = item.me.avatarB64;
    });
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
    });
  }

  // Initialized form
  validation() {
    this.createForm = this.fBuilder.group({
      iTitle: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(100)]],
      iEduLevel: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      iCompetence: ['', [Validators.required,  Validators.minLength(2), Validators.maxLength(20)]],
      iCity: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
      iRangoSalarial: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(60)]],
      iRequiredExperience: ['', [Validators.required]], 
      iRemoto: ['', [Validators.required]],
      iTipoContrato: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(35)]],
      iJornada: ['', [Validators.required, Validators.maxLength(30)]],
      iDescripcio: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]],
      iRequirements: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]]
    });

  }

  // Get competencies
  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => {
        this.competList = result.data.getCompetencies;
      })
    ).subscribe((item) => {
      //console.log(this.competList);
    });
  }

  get errorCtr() {
    return this.createForm.controls;
  }

  // Submit Form
  async onSubmit() {
    this.isSubmitted = true;
    if (!this.createForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      await this.insertedTags(this.createForm.value.iCompetence);
      for (let file of this.filenames) {
        if (file.split('.').pop() === 'pdf') {
        }
        else {
          this.loadingCtrl.dismiss();
          this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'dark',
              message: value,
              duration: 3000,
              position: 'middle'
            });
            await toast.present();

          });
          return;
        }
      }

      this.loadingCtrl.create({
        message: 'Editando oferta...'
      }).then(async res => {
        res.present();
        console.log('Guardando datos...');
        await this.createNewOffer(
          this.userID,
          this.createForm.value.iTitle,
          this.createForm.value.iEduLevel,
          this.newOfferCompetsList,
          this.createForm.value.iCity,
          this.createForm.value.iJornada,
          this.createForm.value.iRangoSalarial,
          this.createForm.value.iRemoto,
          this.addEnroll,
          this.createForm.value.iTipoContrato,
          this.createForm.value.iDescripcio,
          this.createForm.value.iRequirements,
          new Date().toISOString(),
          this.createForm.value.iRequiredExperience,
          OfferStatus.ACTIVE,
          this.filenames

        );
        this.loadingCtrl.dismiss();
      });
    }
  }

  // Created new competence if not exist yet
  async insertedTags(competencies) {
    for (let i = 0; i < competencies.length; i++) {
      if (competencies[i]._id) {
        this.newOfferCompetsList.push({ "_id": competencies[i]._id, "name": competencies[i].value })
      } else {
        const competency = this.competList.find(c => c.name.trim().toLowerCase() === competencies[i].value.trim().toLowerCase())
        if (!competency) {
          const newCompetency = await this.createNewCompetence(competencies[i].value);
          this.newOfferCompetsList.push({ "_id": newCompetency._id, "name": newCompetency.name });
        } else {
          this.newOfferCompetsList.push({ "_id": competency._id, "name": competency.name });
        }
      }
    }
  }

  // Create new competence:
  async createNewCompetence(iCompetence: any) {
    return firstValueFrom(this.competService.mCreateCompetence(iCompetence));
  }


  /**
   * Call to Create Offer Service.
   * @param uId
   * @param iTitle
   * @param iCity
   * @param iJornada
   * @param iRango
   * @param iRemoto
   * @param iEnroll
   * @param iContrato
   * @param iDate
   */
  createNewOffer(uId: any, iTitle: any, iEduLevel: any, iCompetence: any, iCity: any, iJornada: any, iRango: any, iRemoto: any, iEnroll: any, iContrato: any, iDescripcio: any, iRequirements: any, iDate: string, iRequiredExperience: RequiredExperience, status: OfferStatus, iFiles: any) {
    this.compOfService.mCreateOffer(uId, iTitle, iEduLevel, iCompetence, iCity, iJornada, iRango, iRemoto, iEnroll, iContrato, iDescripcio, iRequirements, iDate, iRequiredExperience, status, iFiles)
    .subscribe((item) => {
      console.log('Nueva oferta creada!');
      this.offerInfo = item.data;
      this.offerID = this.offerInfo.createOffer._id;
      for (let file of this.files){
        this.compOfService.qGetUploadOfferUrl(this.offerID, file.name).subscribe(response => {
            const formData = new FormData();
            formData.append('file', file)
            fetch(response.data.uploadOfferUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
              if (response.ok) {
                console.log('Archivo con nombre', file.name ,'subido correctamente!' )
              }
            });
          });
      } 
    });
    this.dismissEditModal();
  }

  // Delete function to competencies/tags.
  removeCompetence(item) {
    const tags = this.createForm.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  matchCompetencies = (query: string, item: any): boolean => {
    return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
  }

  onSelectFile(event): any {
    this.filenames.push(event.target.files[0].name)
    this.files.push(event.target.files[0]);

  }

  onDeleteFile(file): any {
    const index = this.filenames.indexOf(file);
    this.filenames.splice(index, 1);
  }


  /**
   * Close modal when create
   */
   async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }

}
