import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { AvatarModalPage } from 'src/app/shared/modals/avatar-modal/avatar-modal.page';
import { FileModalPage } from 'src/app/shared/modals/file-modal/file-modal.page';
import { AuthService } from '../../services/auth.service';
import { ValuesModalPage } from '../../shared/modals/values-modal/values-modal.page';
import { EditUserPage } from '../edit-user/edit-user.page';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  skillsList: any[] = [];
  userInfo: User;
  userSkills: any[] = [];
  skillsName: any;
  skillsIco: any[] = [];
  compet: any;
  userCompets: any[] = [];
  sectorName = '';
  avatar: string; 
  userID: string;

  constructor(
              private modalController: ModalController,
              private routerOutlet: IonRouterOutlet,
              private uService: UserService,
              private auth: AuthService,
              private router: Router) {}

  ngOnInit() {
    this.getLoggedUser();

    if(!this.router.getCurrentNavigation().extras.state){   
    }
    else{
      this.router.navigate(['/mp1'], {state: {button: '1'}});
    }
  }


  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
      this.setSoftSkills(item.me.softSkills);
      this.userCompets = item.me.competencies;
    });
  }



  setSoftSkills(softSkills) {
    this.userSkills = softSkills;
    this.useSessionStorage(this.userSkills);
    this.skillsIco = [];
    softSkills.forEach(skill => {
      const nameClass = skill.name.replace(/ /g, '_');
      this.skillsName = nameClass.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
      this.skillsIco.push({
        sid: skill._id,
        sname: skill.name,
        sClass: this.skillsName
      });
    })
  }

  useSessionStorage(skills) {
    sessionStorage.setItem('uSelectedSkills', JSON.stringify(skills));
  }

  logOut() {
    this.auth.onLogout();
    this.router.navigate(['/login']);
  }

  /**
   * Call modal to change values from profile.
   * @returns
   */
  async openModal() {
    const modal = await this.modalController.create({
      component: ValuesModalPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    return await modal.present();
  }

  /**
   * Call modal to edit user profile
   */
  async editModal() {
    const editModal = await this.modalController.create({
      component: EditUserPage,
      componentProps: {
        userData: this.userInfo
      },
      animated: true,
      cssClass: 'modalCss'
    });
    await editModal.present();
  }


    async editAvatarModal() {
      const modal = await this.modalController.create({
        component: AvatarModalPage,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        animated: true,
        cssClass: 'modalCss'
      });
      return await modal.present();
    }

    async uploadCVModal() {
      const modal = await this.modalController.create({
        component: FileModalPage,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        animated: true,
        cssClass: 'modalCss',
        componentProps: {
          mode: 'CV'
        }
      });
      await modal.present();
    }

    async uploadCoverLetterModal() {
      const modal = await this.modalController.create({
        component: FileModalPage,
        swipeToClose: true,
        presentingElement: this.routerOutlet.nativeEl,
        animated: true,
        cssClass: 'modalCss',
        componentProps: {
          mode: 'Cover_Letter'
        }
      });
      await modal.present();
    }

    async downloadCV() {
      this.uService
        .qGetDownloadCVUrl()
        .subscribe(response => {
          window.open(response.data.downloadCvUrl.presignedUrl);
        });
    }

    async downloadCoverLetter() {
      this.uService
        .qGetDownloadCoverLetterUrl()
        .subscribe(response => {
          window.open(response.data.downloadCoverLetterUrl.presignedUrl);
        })
    }
}
