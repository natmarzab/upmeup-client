import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IonRouterOutlet, ModalController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { AvatarModalPage } from 'src/app/shared/modals/avatar-modal/avatar-modal.page';
import { AuthService } from '../../services/auth.service';
import { ValuesModalPage } from '../../shared/modals/values-modal/values-modal.page';
import { EditUserPage } from '../edit-user/edit-user.page';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.page.html',
  styleUrls: ['./company-profile.page.scss'],
})
export class CompanyProfilePage implements OnInit {
  userInfo: User;
  userSkills: any[] = [];
  sectorName = '';
  skillsName: any;
  skillsIco: any[] = [];
  userType: string;
  avatar: string;

  constructor(private modalController: ModalController,
              private routerOutlet: IonRouterOutlet,
              private uService: UserService,
              private auth: AuthService,
              private router: Router) {}
  

  ngOnInit() {
    this.getLoggedUser();

    if(!this.router.getCurrentNavigation().extras.state){   
    }
    else {
      this.router.navigate(['/mp1'], {state: {button: '1'}});
    }
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type
      this.setSoftSkills(item.me.softSkills);
      this.avatar = item.me.avatarB64;
      this.sectorName = item.me.sector.name
      // console.log(`Current userInfo: ${JSON.stringify(this.userInfo)}`) // debug
      // console.log(`Current userInfo: email: ${this.userInfo.email}; website: ${this.userInfo.website}`)
      
    });
  }


  setSoftSkills(softSkills) {
    this.userSkills = softSkills;
    this.useSessionStorage(this.userSkills);
    this.skillsIco = [];
    softSkills.forEach(skill => {
      const nameClass = skill.name.replace(/ /g, '_');
      this.skillsName = nameClass.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
      this.skillsIco.push({
        sid: skill._id,
        sname: skill.name,
        sClass: this.skillsName
      });
    })
  }

  useSessionStorage(skills) {
    sessionStorage.setItem('uSelectedSkills', JSON.stringify(skills));
  }

  logOut() {
    this.auth.onLogout();
    this.router.navigate(['/login']);
  }

  /**
  * Call modal to change values from profile.
  * @returns
  */
  async openModal() {
    const modal = await this.modalController.create({
      component: ValuesModalPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl
    });
    return await modal.present();
  }

  /**
  * Call modal to edit user profile 
  */
  async editModal() {
    const editModal = await this.modalController.create({
      component: EditUserPage,
      componentProps: {
        userData: this.userInfo
      },
      animated: true,
      cssClass: 'modalCss'
    });

    await editModal.present();
  }

   /**
   * Call modal to change avatar from profile.
   * @returns
   */
   async editAvatarModal() {
    const modal = await this.modalController.create({
      component: AvatarModalPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
    });
    return await modal.present();
  }
  
  goToWebsite(){
    // console.log(`Current userInfo: ${JSON.stringify(this.userInfo)}`)
    const website = this.userInfo.website
    
    const url = website.substring(0,4) === 'http' // http / https
        ? this.userInfo.website
        : `https://`+ this.userInfo.website // default to https
    // console.log(`goToLink; ${url}`)
    window.open(url, "_blank");
  }

}
