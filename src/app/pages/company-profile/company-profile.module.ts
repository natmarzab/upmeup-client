import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TagInputModule } from 'ngx-chips';

import { CompanyProfilePageRoutingModule } from './company-profile-routing.module';
import { CompanyProfilePage } from './company-profile.page';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { HttpClient } from '@angular/common/http';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CompanyProfilePageRoutingModule,
    TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  declarations: [CompanyProfilePage],
  providers: [MultilanguageComponent]
})
export class CompanyProfilePageModule {}
