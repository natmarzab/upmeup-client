/* eslint-disable prefer-const */
/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, ModalController, ToastController } from '@ionic/angular';
import { map } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { CompetenceService } from 'src/app/services/competence.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList, languageList } from '../../utils/constants';
import { MatchService } from 'src/app/services/match.service';
import { firstValueFrom } from 'rxjs';

interface sector {
  _id:string,
  name: string
}

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.page.html', 
  styleUrls: ['./edit-user.page.scss'],
})
export class EditUserPage implements OnInit {
  languageList = languageList;
  cityList = cityList

  userID = sessionStorage.getItem('userid');
  userType = '0';
  editUserForm: FormGroup;
  isSubmitted = false;
  sectorsList: any[] = [];

  competList: any[] = [];
  userCompetIDs: any[] = [];
  selectedCompet: any[] = [];
  newUserCompets: any[] = [];
  newUserCompetsList: any[] = [];
  nameNewCompet: any[] = [];
  selectedLang= "";
  languageListTranslated: any[] = [];
  finalLangList: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  sectorListTranslated: any[] = [];
  finalSectorList: any[] = [];
  avatar: string;
  usectors: sector[] = [];
  userInfo: User;
  error : any;
  errorMessage: any;
  createNewCompet: Boolean;
  newCompetCreated: Boolean;


 // updateUserData: User;
  @Input() userData: User;

  constructor(
          private uService: UserService,
          private sectService: SectorsService,
          public fBuilder: FormBuilder,
          private alrtController: AlertController,
          private mdlController: ModalController,
          private competService: CompetenceService,
          public loadingCtrl: LoadingController,
          private translateList: MultilanguageComponent,
          private toastCtrl: ToastController,
          private mService: MatchService) {}

  ngOnInit() {
    this.userType = this.userData.type;
    if (this.userType === '1') {
        this.userCompetIDs = this.userData.competencies.map(c => c._id);
    }
    this.getAvatar();
    this.initForm();
    this.qGetSectors();
    this.qGetCompetencies();
    this.selectedCompet = this.userData.competencies;
    this.setValues(this.userData);
    this.finalCityList = this.translateList.translateCityList();
    this.finalLangList = this.translateList.translateLangList();
    this.getLoggedUser();
    
  }

  getAvatar(){
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.avatar = item.me.avatarB64;
    });
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;
    });
  }

  //Get sectors Info
  qGetSectors() {
    this.sectService.qGetAllSectors().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sectorsList =item.getSectors;
      this.finalSectorList = this.translateList.translateSectorList(this.sectorsList)
    })
  } 

  getLang(){
    return localStorage.getItem('lang')
  }

  // Get competence List
  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.competList = item.getCompetencies
    });
  }

  get errorCtr() {
    return this.editUserForm.controls;
  }

  /**
   * Initialized form
   */
   initForm(){
    if(this.userType === '2') { // Company
      this.editUserForm = this.fBuilder.group({
        iName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
        iWebsite: ['', [Validators.minLength(0), Validators.maxLength(100)]],
        iEmail: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(60)]],
        iCity: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
        iSector: ['', [Validators.required]],
        iLastJob: ['', [Validators.required,  Validators.minLength(20), Validators.maxLength(1000)]],
        iExp: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(2)]]
      });
    } else if(this.userType === '1') {
      this.editUserForm = this.fBuilder.group({
        iName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
        iSurname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
        iEmail: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(60)]],
        iCity: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(30)]],
        iSector: ['', [Validators.required]],
        iEduc: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
        iJobPos: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(70)]],
        iLastJob: ['', [Validators.required,  Validators.minLength(20), Validators.maxLength(1000)]],
        iExp: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(2)]],
        iLang: ['', [Validators.required,  Validators.minLength(1), Validators.maxLength(30)]],
        iCompetence: ['', [Validators.minLength(2), Validators.maxLength(20)]],
        iVideo: [''],
        iCoverLetter:['', [Validators.maxLength(2000)]]
      });
    }
  }

  /**
   * Set values form 'father' component.
   * @param infoOffer
  */
  setValues(infoUser) {
    if(this.userType === '2') { // Company
      this.userID = infoUser._id;
      this.editUserForm.get('iName').setValue(infoUser.name);
      this.editUserForm.get('iEmail').setValue(infoUser.email);
      this.editUserForm.get('iCity').setValue(infoUser.city);
      this.editUserForm.get('iWebsite').setValue(infoUser.website);
      this.editUserForm.get('iSector').setValue(infoUser.sector.map(sector => sector._id));
      this.editUserForm.get('iLastJob').setValue(infoUser.lastJobTasks);
      this.editUserForm.get('iExp').setValue(infoUser.experience);
    } else if(this.userType === '1') {
      this.userID = infoUser._id;
      this.editUserForm.get('iName').setValue(infoUser.name);
      this.editUserForm.get('iSurname').setValue(infoUser.surname);
      this.editUserForm.get('iEmail').setValue(infoUser.email);
      this.editUserForm.get('iCity').setValue(infoUser.city);
      this.editUserForm.get('iSector').setValue(infoUser.sector.map(sector => sector._id));
      this.editUserForm.get('iEduc').setValue(infoUser.eduLevel);
      this.editUserForm.get('iJobPos').setValue(infoUser.jobPosition);
      this.editUserForm.get('iLastJob').setValue(infoUser.lastJobTasks);
      this.editUserForm.get('iExp').setValue(infoUser.experience);
      this.editUserForm.get('iLang').setValue(infoUser.languages);
      this.editUserForm.get('iCompetence').setValue(infoUser.competencies);
      this.editUserForm.get('iVideo').setValue(infoUser.video);
      this.editUserForm.get('iCoverLetter').setValue(infoUser.coverLetter);
    }
  }

  /**
   * Submit Form
   * @returns 
  */
  async onSubmit() {
    this.isSubmitted = true;
    if (!this.editUserForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      if(this.userType === '1') {
        await this.insertedTags(this.editUserForm.value.iCompetence);
      } else { // Company
        this.newUserCompets = [];
        this.editUserForm.value.iSurname = '-';
        this.editUserForm.value.iJobPos = '-';
        this.editUserForm.value.iEduc = '-';
        this.editUserForm.value.iLang = [];
      }

      this.loadingCtrl.create({
        message: 'Desant canvis...'
      }).then(async res => {
        res.present();

        this.editUserForm.value.iSector.forEach(usector => {
          const { _id, name } = this.sectorsList.find(sector => usector === sector._id)
          this.usectors.push({_id, name})
        })


        if (this.editUserForm.value.iEmail !== this.userInfo.email) {
          await this.validateEmail(this.editUserForm.value.iEmail);
        } 
        else {
          await this.editUser(
            this.userID,
            this.editUserForm.value.iName,
            this.editUserForm.value.iSurname,
            this.editUserForm.value.iEmail,
            this.editUserForm.value.iCity,
            this.editUserForm.value.iWebsite,
            this.usectors,
            this.editUserForm.value.iEduc,
            this.editUserForm.value.iJobPos,
            this.editUserForm.value.iLastJob,
            this.editUserForm.value.iExp + "",
            this.editUserForm.value.iLang,
            this.newUserCompetsList,
            this.editUserForm.value.iVideo,
            this.editUserForm.value.iCoverLetter
          )
        }
        this.loadingCtrl.dismiss();
        })
      };
  }
  

  // Created new competence if not exist yet
  async insertedTags(competencies) {
    for (let i = 0; i < competencies.length; i++) {
      if (competencies[i]._id) {
        this.newUserCompetsList.push({ "_id": competencies[i]._id, "name": competencies[i].name })
      } else {
        const competency = this.competList.find(c => c.name.trim().toLowerCase() === competencies[i].name.trim().toLowerCase())
        if (!competency) {
          const newCompetency = await this.createNewCompetence(competencies[i].name);
          this.newUserCompetsList.push({ "_id": newCompetency._id, "name": newCompetency.name });
        } else {
          this.newUserCompetsList.push({ "_id": competency._id, "name": competency.name });
        }
      }
    }
  }

  // Create new competence:
  async createNewCompetence(name: any) {
    return firstValueFrom(this.competService.mCreateCompetence(name));
  }

  async validateEmail(iEmail){ 
    this.uService.mCheckEmail(iEmail).
      subscribe({
        next: () => {
        this.editUser(
            this.userID,
            this.editUserForm.value.iName,
            this.editUserForm.value.iSurname,
            this.editUserForm.value.iEmail,
            this.editUserForm.value.iCity,
            this.editUserForm.value.iWebsite,
            this.usectors,
            this.editUserForm.value.iEduc,
            this.editUserForm.value.iJobPos,
            this.editUserForm.value.iLastJob,
            this.editUserForm.value.iExp + "",
            this.editUserForm.value.iLang,
            this.newUserCompetsList,
            this.editUserForm.value.iVideo,
            this.editUserForm.value.iCoverLetter
        )
        this.error = 'false';
        return;
        },
      
        error: async (error) => {
          this.loadingCtrl.dismiss();
          console.log("El mail ya existe...")
          this.translateList.translate.stream(error.message).subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'danger',
              message: value,
              duration: 3000,
              position: 'bottom'
            });

          toast.present();
          return this.errorMessage;
          });
        }
      })
  };

  editUser(uId: any, iName: any, iSurname: any, iEmail: any, iCity: any, iWebsite: any, iSector: any, iEduc: any, iJobPos: any, iLastJob: any, 
          iExp: any, iLang: any, iCompetence: any, iVideo:any, iCoverLetter: any){
    sessionStorage.setItem('user', iName);
    this.uService.mEditUser(uId, iName, iSurname, iEmail, iCity, iWebsite, iSector, iEduc, iJobPos, iLastJob, iExp, iLang, iCompetence, iVideo, iCoverLetter)
    .subscribe((response) => {
      console.log('Profile edited!');
      this.calculateMatch(uId);
    });

    this.dismissEditModal();
  }

  calculateMatch(userID){
    this.mService.mCalculateMatchByUser(userID).subscribe(() => {
      console.log('Match calculated!');
    });

  }

  matchCompetencies = (query: string, item: any): boolean => {
    return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
  }

  // Delete function to competencies/tags.
  removeCompetence(item) {
    const tags = this.editUserForm.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  addExperience(experience: number) {
    if (+this.editUserForm.value.iExp > 0 || experience > 0)
      this.editUserForm.controls['iExp'].setValue((+this.editUserForm.value.iExp) + experience);
  }

  /**
  * Close modal when update
  */
   async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }

}
