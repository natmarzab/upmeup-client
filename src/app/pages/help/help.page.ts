import { Component, OnInit } from "@angular/core";
import { map } from "rxjs";
import { User } from "src/app/models/user";
import { UserService } from "src/app/services/user.service";

@Component({
    selector: 'app-help',
    templateUrl: './help.page.html',
    styleUrls: ['./help.page.scss'],
  })
  export class HelpPage implements OnInit{
    userInfo: User;
    avatar: string; 
    userType = '0';
    
    constructor(private uService: UserService) {}



    ngOnInit(): void {
        this.getLoggedUser();
    }

    getLoggedUser() {
        this.uService.qGetMe().valueChanges.pipe(
          map(result => result.data)
        ).subscribe((item) => {
          this.userInfo = item.me;
          this.avatar = item.me.avatarB64;
          this.userType = item.me.type;
        });
      }

  }