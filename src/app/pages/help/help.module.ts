import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { TagInputModule } from "ngx-chips";
import { MenuMobileModule } from "src/app/shared/menu-mobile/menu-mobile.module";
import { MultilanguageModule } from "src/app/shared/multilanguage/multilanguage.module";
import { ToolbarModule } from "src/app/shared/toolbar/toolbar.module";
import { HelpPageRoutingModule } from "./help-routing.module";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { HttpLoaderFactory } from "src/app/app.module";
import { HelpPage } from "./help.page";
import { MultilanguageComponent } from "src/app/shared/multilanguage/multilanguage.component";

@NgModule({
    imports: [
      MultilanguageModule,
      ToolbarModule,
      MenuMobileModule,
      CommonModule,
      TagInputModule,
      IonicModule,
      HelpPageRoutingModule,
      //ngx-translate and loader module
      HttpClientModule,
      TranslateModule.forChild({
          loader: {
              provide: TranslateLoader,
              useFactory: HttpLoaderFactory,
              deps: [HttpClient]
          }
      })
    ],
    declarations: [HelpPage], //, ProgressBarComponent]
    providers: [MultilanguageComponent]
  })
  export class HelpPageModule {}