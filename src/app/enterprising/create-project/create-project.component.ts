import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { firstValueFrom, map } from 'rxjs';
import { User } from 'src/app/models/user';
import { CompetenceService } from 'src/app/services/competence.service';
import { MatchService } from 'src/app/services/match.service';
import { ProjectService } from 'src/app/services/project.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { SoftskillsService } from 'src/app/services/softskills.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';

interface sector {
  _id:string,
  name: string
}

interface softskill {
  _id:string,
  name: string
}

interface admin {
  _id: string,
  email: string
}

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {

  editForm: FormGroup;
  avatar: string; 
  userInfo: User;
  competList: any[] = [];
  filenames: any[] = [];
  files: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  sectorsList: any[] = [];
  sectorListTranslated: any[] = [];
  finalSectorList: any[] = [];
  finalSkillsList: {name: any , output: any, _id: any, label: any} [] = [{"name": "", "_id": "", "output": "", "label":""}];
  skillsList: any[] = [];
  isSubmitted = false;
  cDate: string;
  offerCompetsIds: any[] = [];
  newProjectCompetsList: any[] = [];
  nameNewCompet: any = [];
  userID: string;
  offerCompets: any[] = [];
  psectors: sector[] = [];
  psoftskills: softskill[] = [];
  usersList: any[] = [];
  admins: admin[] = [];
  projectInfo: any;
  projectID: string;
  newAdmins: any[] = [];
  error: Boolean;
  createNewCompet: Boolean;
  newCompetCreated: Boolean;

  @Input() mode: String;

  constructor(public fBuilder: FormBuilder,
              public uService: UserService,
              public competService: CompetenceService,
              public mController: ModalController,
              public sectService: SectorsService,
              private translateList: MultilanguageComponent,
              public softSkillService: SoftskillsService,
              private toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              public pService: ProjectService,
              public mService: MatchService) { }

  ngOnInit(): void {
    this.userID = sessionStorage.getItem('userid');
    this.getLoggedUser();
    this.qGetSectors();
    this.qGetCompetencies();
    this.finalCityList = this.translateList.translateCityList();
    this.qsoftSkillsQuery();
    this.validation();
    this.qGetAllUsers();
    
  }

  qGetAllUsers(){
    this.uService.qGetAllUsers().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.usersList = item.getUsers.map(u => u.email);
    })
  }

  //Init form 
  validation() {
    this.editForm = this.fBuilder.group({
      iTitle: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      iDescription: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]],
      iSector: ['', [Validators.required]],
      iCompetence: ['', [Validators.required,  Validators.minLength(2), Validators.maxLength(20)]],
      iCity: [''],
      iValues: ['', [Validators.required]],
      iStatus: ['', [Validators.required]],
      iAdmins: [''] 
    });
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.avatar = item.me.avatarB64;  
    });
  }

  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => {
        this.competList = result.data.getCompetencies;       
      })
    ).subscribe((item) => {
    });      
  }

  qGetSectors() {
    this.sectService.qGetAllSectors().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sectorsList =item.getSectors;
      this.finalSectorList = this.translateList.translateSectorList(this.sectorsList)
    })
  } 

  qsoftSkillsQuery() {
    this.softSkillService.qGetAllSkills().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.skillsList = item.getSoftskills;
      this.finalSkillsList = this.translateList.translateSkillsLists(this.skillsList)
    });

  }

    // Delete function to competencies/tags.
    removeCompetence(item) {
      const tags = this.editForm.value.iCompetence;
      const index = tags.indexOf(item);
      tags.splice(index, 1);
      this.editForm.get('iCompetence').setValue(tags);
    }

    removeEmail(item) {
      const tags = this.editForm.value.iAdmins;
      const index = tags.indexOf(item);
      tags.splice(index, 1);
    }

    onSelectFile(event): any {
      this.filenames.push(event.target.files[0].name)
      this.files.push(event.target.files[0]);
  
    }
  
    onDeleteFile(file): any {
      const index = this.filenames.indexOf(file);
      this.filenames.splice(index, 1);
    }

    get errorCtr() {
      return this.editForm.controls;
    }


  // Submit Form
  async onSubmit() {
    this.isSubmitted = true;
    if (!this.editForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      await this.insertedTags(this.editForm.value.iCompetence);
      this.newAdmins.push({"_id": this.userID, "email": this.userInfo.email })

      if(this.editForm.value.iAdmins){
        this.error = false;

        for (let admin of await this.findUser(this.editForm.value.iAdmins)){
          this.newAdmins.push(admin);
        } 
      } 

      for (let file of this.filenames) {
        if (file.split('.').pop() === 'pdf') {
        }
        else {
          this.loadingCtrl.dismiss();
          this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'dark',
              message: value,
              duration: 3000,
              position: 'middle'
            });
            await toast.present();

          });
          return;
        }
      }


      if (this.error !== true){
        this.loadingCtrl.create({
          message: 'Creating project...'
        }).then(async res => {
          res.present();
          console.log('Saving info...!');
        
          this.editForm.value.iSector.forEach(psector => {
            const { _id, name } = this.sectorsList.find(sector => psector === sector._id)
            this.psectors.push({_id, name})
          })

          this.editForm.value.iValues.forEach(psoftskill => {
            const { _id, name } = this.skillsList.find(softskill => psoftskill === softskill._id)
            this.psoftskills.push({_id, name})
          })
          
          await this.createNewProject(
              this.userID,
              this.editForm.value.iTitle,
              this.editForm.value.iDescription,
              this.psectors,
              this.newProjectCompetsList,
              this.editForm.value.iCity,
              this.psoftskills,
              this.editForm.value.iStatus,
              new Date().toISOString(),
              this.filenames,
              this.newAdmins
          );
            
          this.loadingCtrl.dismiss();
      });
    }
  }}

  // Created new competence if not exist yet
  async insertedTags(competencies) {
    for (let i = 0; i < competencies.length; i++) {
      if (competencies[i]._id) {
        this.newProjectCompetsList.push({ "_id": competencies[i]._id, "name": competencies[i].value })
      } else {
        const competency = this.competList.find(c => c.name.trim().toLowerCase() === competencies[i].value.trim().toLowerCase())
        if (!competency) {
          const newCompetency = await this.createNewCompetence(competencies[i].value);
          this.newProjectCompetsList.push({ "_id": newCompetency._id, "name": newCompetency.name });
        } else {
          this.newProjectCompetsList.push({ "_id": competency._id, "name": competency.name });
        }
      }
    }
  }

  // Create new competence:
  async createNewCompetence(iCompetence: any) {
    return firstValueFrom(this.competService.mCreateCompetence(iCompetence));
  }


    matchCompetencies = (query: string, item: any): boolean => {
      return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
    }

    matchEmail = (query: string, item: any): boolean => {
      return item.name.includes(query.toLowerCase().trim());
    }
  


    async findUser(emails){
      let newAdmins = []

      for (let email of emails){
        const index = this.usersList.findIndex(
          object => object === email.name
        );

        if(index === -1){
          console.log('No se encuentra el email "', email.name, '"')
          this.createAlert();
          this.error = true;
        } else {
          let result = (await firstValueFrom( this.uService.qGetUserByEmail(email.name).valueChanges)).data.getUserByEmail;
          let newAdmin = {"_id": result._id, "email": result.email}
          newAdmins.push(newAdmin);
        }
      }
      return newAdmins;
    }

    async createAlert(){
      const toast = await this.toastCtrl.create({
        color: 'dark',
        message: 'Por favor, asegurate de que todos los emails introducidos están registrados en UpmeUp',
        duration: 3000,
        position: 'middle'
      });
  
      await toast.present();
      return;
    }

     /**
   * Call to Create Offer Service.
   * @param uId
   * @param iTitle
   * @param iCity
   * @param iJornada
   * @param iRango
   * @param iRemoto
   * @param iEnroll
   * @param iContrato
   * @param iDate
   */
  createNewProject(uId: any, iTitle: any, iDescription, iSector, iCompetence: any, iCity: any, iValues: any, iStatus: string, iDate: string, iFiles: any, iAdmins: any) {
    this.pService.mCreateProject(uId, iTitle, iDescription, iSector, iCompetence, iCity, iValues, iStatus, iDate, iFiles, iAdmins)
    .subscribe((item) => {
      console.log('New project created!');
      this.projectInfo = item.data;
      this.projectID = this.projectInfo.createProject._id
      this.calculateMatch(this.projectID);
      for (let file of this.files){
        this.pService.qGetUploadProjectUrl(this.projectID, file.name).subscribe(response => {
            const formData = new FormData();
            formData.append('file', file)
            fetch(response.data.uploadProjectUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
              if (response.ok) {
                console.log('Archivo con nombre', file.name ,'subido correctamente!' )
              }
            });
          });
      }   
    });
    this.dismissModal();
  }

  calculateMatch(projectID){
    this.mService.mCalculateMatchByProject(projectID).subscribe(() => {
      console.log('Match calculated!');
    });

  }

    @HostListener('window:popstate', ['$event'])
    dismissModal() {
      this.mController.dismiss();
    }

}
