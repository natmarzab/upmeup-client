import { NgModule } from "@angular/core";
import { CreateProjectComponent } from "./create-project.component";
import { MultilanguageModule } from "src/app/shared/multilanguage/multilanguage.module";
import { CommonModule } from "@angular/common";
import { TagInputModule } from "ngx-chips";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { CreateProjectComponentRoutingModule } from "./create-project-routing.module";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { HttpLoaderFactory } from "src/app/app.module";
import { HttpClient } from "@angular/common/http";


@NgModule({
   imports: [
    MultilanguageModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CreateProjectComponentRoutingModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
   ],
   declarations: [CreateProjectComponent]
})
export class CreateProjectComponentModule {}
