import { CommonModule } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TagInputModule } from "ngx-chips";
import { HttpLoaderFactory } from "src/app/app.module";
import { MultilanguageModule } from "src/app/shared/multilanguage/multilanguage.module";
import { EditProjectComponentRoutingModule } from "./edit-project-routing.module";
import { EditProjectComponent } from "./edit-project.component";


@NgModule({
   imports: [
    MultilanguageModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    EditProjectComponentRoutingModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
   ],
   declarations: [EditProjectComponent]
})
export class EditProjectComponentModule {}