import { Component, HostListener, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { firstValueFrom, map } from 'rxjs';
import { OfferStatus } from 'src/app/models/offer-status.model';
import { Project } from 'src/app/models/project';
import { User } from 'src/app/models/user';
import { CompetenceService } from 'src/app/services/competence.service';
import { MatchService } from 'src/app/services/match.service';
import { ProjectService } from 'src/app/services/project.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { SoftskillsService } from 'src/app/services/softskills.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';

interface sector {
  _id:string,
  name: string
}

interface softskill {
  _id:string,
  name: string
}

@Component({
  selector: 'app-edit-project',
  templateUrl: './edit-project.component.html',
  styleUrls: ['./edit-project.component.scss']
})
export class EditProjectComponent implements OnInit {

  avatar: string;
  userInfo: User;
  userID: string; 
  editForm: FormGroup;
  projectCompetIDs: any[] = [];
  selectedCompet: any[] = [];
  competList: any[] = [];
  sectorsList: any[] = [];
  finalSectorList: any[] = [];
  skillsList: any[] = [];
  finalSkillsList: {name: any , output: any, _id: any, label: any} [] = [{"name": "", "_id": "", "output": "", "label":""}];
  projectID: any;
  finalCityList: any[] = [];
  filenames: any[] = [];
  files: any[] = [];
  userSkills: any[] = [];
  nameNewCompet: any[] = [];
  newProjectCompetsList: any[] = [];
  psectors: sector[] = [];
  psoftskills: softskill[] = [];
  showOption: any = 'false';
  state: any;
  admins: any[] = [];
  usersList: any[] = [];
  addAdmins: any[] = [];
  newAdmins: any[] = [];
  isSubmitted: boolean;
  error: boolean;
  
  @Input() projectData: Project;

  constructor(private uService: UserService,
              public fBuilder: FormBuilder,
              private competService: CompetenceService,
              private sectService: SectorsService,
              private softSkillService: SoftskillsService,
              private translateList: MultilanguageComponent,
              private mdlController: ModalController,
              private toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private pService: ProjectService,
              public router: Router,
              private mService: MatchService) { }

  ngOnInit(){
    this.getLoggedUser();
    this.qGetAllUsers();
    this.initForm();
    this.projectCompetIDs = this.projectData.competencies.map(c => c._id);
    this.selectedCompet = this.projectData.competencies;
    this.qGetCompetencies();
    this.qGetSectors();
    this.qGetSoftSkills();
    this.qGetState();
    this.finalCityList = this.translateList.translateCityList();



    for (let admin of this.projectData.admins){
      this.admins.push({'_id': admin._id, 'email': admin.email});
    }

    this.setValues(this.projectData);

    for (let file of this.projectData.files){
      this.filenames.push(file);
    }
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userID = item.me._id
      this.avatar = item.me.avatarB64;
      this.userSkills = item.me.softSkills;
    });
  }

  qGetAllUsers(){
    this.uService.qGetAllUsers().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.usersList = item.getUsers.map(u => u.email);
    })
  }

  initForm() {
    this.editForm = this.fBuilder.group({
      iTitle: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]],
      iDescription: ['', [Validators.required,  Validators.minLength(40), Validators.maxLength(1000)]],
      iSector: ['', [Validators.required]],
      iCompetence: ['', [Validators.required,  Validators.minLength(2), Validators.maxLength(20)]],
      iCity: [''],
      iValues: ['', [Validators.required]],
      iStatus: ['', [Validators.required]],
      iAdmins: [''] 
    });
  }

  qGetCompetencies() {
    this.competService.qGetCompetencies().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.competList = item.getCompetencies;
    });
  }

  qGetSectors() {
    this.sectService.qGetAllSectors().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sectorsList =item.getSectors;
      this.finalSectorList = this.translateList.translateSectorList(this.sectorsList)
    })
  } 

  qGetSoftSkills() {
    this.softSkillService.qGetAllSkills().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.skillsList = item.getSoftskills;
      this.finalSkillsList = this.translateList.translateSkillsLists(this.skillsList)
    });

  }

  qGetState(){
    this.pService.qGetProject(this.projectData._id).valueChanges.pipe(
      map(result => result.data.getProject)
    ).subscribe((item) => {
      this.state = item.state
    })
  }

  setValues(infoProject) {
    this.projectID = infoProject._id;
    this.editForm.get('iTitle').setValue(infoProject.title);
    this.editForm.get('iDescription').setValue(infoProject.description);
    this.editForm.get('iSector').setValue(infoProject.sector.map(sector => sector._id));
    this.editForm.get('iCompetence').setValue(infoProject.competencies);
    this.editForm.get('iCity').setValue(infoProject.city);
    this.editForm.get('iValues').setValue(infoProject.softSkills.map(value => value._id));
    this.editForm.get('iStatus').setValue(infoProject.status);
    this.editForm.get('iAdmins').setValue(this.admins);
  }

  onSelectFile(event): any {
    this.filenames.push(event.target.files[0].name)
    this.files.push(event.target.files[0]);

  }

  onDeleteFile(file): any {
    const index = this.filenames.indexOf(file);
    this.filenames.splice(index, 1);
  }


  //Submit Form
  async onSubmit() {
    this.isSubmitted = true;
    if (!this.editForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      await this.insertedTags(this.editForm.value.iCompetence);
      for (let file of this.filenames) {
        if (file.split('.').pop() === 'pdf') {
        }
        else {
          this.loadingCtrl.dismiss();
          this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'dark',
              message: value,
              duration: 3000,
              position: 'middle'
            });
            await toast.present();

          });
          return;
        }
      }

      this.admins = [];

      if(this.editForm.value.iAdmins){
        this.error = false;

        for (let admin of await this.findUser(this.editForm.value.iAdmins)){
          this.newAdmins.push(admin);
        }  
      }

      if (this.error !== true){
        this.loadingCtrl.create({
          message: 'Editando proyecto...'
        }).then(async res => {
          res.present();
          console.log('Guardando datos...');
  
          this.editForm.value.iSector.forEach(psector => {
            const { _id, name } = this.sectorsList.find(sector => psector === sector._id)
            this.psectors.push({_id, name})
          })
  
          this.editForm.value.iValues.forEach(psoftskill => {
            const { _id, name } = this.skillsList.find(softskill => psoftskill === softskill._id)
            this.psoftskills.push({_id, name})
          })
            await this.editProject(
              this.projectID,
              this.userID,
              this.editForm.value.iTitle,
              this.editForm.value.iDescription,
              this.psectors,
              this.newProjectCompetsList,
              this.editForm.value.iCity,
              this.psoftskills,
              this.editForm.value.iStatus,
              this.filenames,
              this.newAdmins
          );
          this.loadingCtrl.dismiss();
        });
      }    
    }
  }

  // Created new competence if not exist yet
  async insertedTags(competencies) {
    for (let i = 0; i < competencies.length; i++) {
      if (competencies[i]._id) {
        this.newProjectCompetsList.push({ "_id": competencies[i]._id, "name": competencies[i].name })
      } else {
        const competency = this.competList.find(c => c.name.trim().toLowerCase() === competencies[i].name.trim().toLowerCase())
        if (!competency) {
          const newCompetency = await this.createNewCompetence(competencies[i].name);
          console.log(newCompetency);
          this.newProjectCompetsList.push({ "_id": newCompetency._id, "name": newCompetency.name });
        } else {
          this.newProjectCompetsList.push({ "_id": competency._id, "name": competency.name });
        }
      }
    }
  }

  // Create new competence:
  async createNewCompetence(iCompetence: any) {
    return firstValueFrom(this.competService.mCreateCompetence(iCompetence));
  }


  matchCompetencies = (query: string, item: any): boolean => {
    return this.translateList.translate.instant(item.name).toLowerCase().trim().includes(query.toLowerCase().trim());
  }

  matchEmail = (query: string, item: any): boolean => {
    return item.email.includes(query.toLowerCase().trim());
  }

  removeCompetence(item) {
    const tags = this.editForm.value.iCompetence;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
    this.editForm.get('iCompetence').setValue(tags);
  }

  removeEmail(item) {
    const tags = this.editForm.value.iAdmins;
    const index = tags.indexOf(item);
    tags.splice(index, 1);
  }

  async findUser(emails){
    let newAdmins = this.admins;

    for (let email of emails){
      const index = this.usersList.findIndex(
        object => object === email.email
      );

        if(index === -1){
          console.log('No se encuentra el email "', email.email, '"')
          this.createAlert();
          this.error = true;
          return;
        } else {
          let result = (await firstValueFrom( this.uService.qGetUserByEmail(email.email).valueChanges)).data.getUserByEmail;
          let newAdmin = {"_id": result._id, "email": result.email}
          newAdmins.push(newAdmin);
        }   

    }
    return newAdmins;
  }

  

  async createAlert(){
    const toast = await this.toastCtrl.create({
      color: 'dark',
      message: 'Por favor, asegurate de que todos los emails introducidos están registrados en UpmeUp',
      duration: 3000,
      position: 'middle'
    });

    await toast.present();
    return;
  }

  editProject(pId: any, uId: any, iTitle: any, iDescription, iSector, iCompetence: any, iCity: any, iValues: any, iStatus: string, iFiles: any, iAdmins: any){
    this.pService.mEditProject(pId, uId, iTitle, iDescription, iSector, iCompetence, iCity, iValues, iStatus, iFiles, iAdmins)
    .subscribe((response) => {
      console.log('Proyecto editado!');
      this.calculateMatch(pId);
      for (let file of this.files){
        this.pService.qGetUploadProjectUrl(pId, file.name).subscribe(response => {
            const formData = new FormData();
            formData.append('file', file)
            fetch(response.data.uploadProjectUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
              if (response.ok) {
                console.log('Archivo con nombre', file.name ,'subido correctamente!' )
              }
            });
          });
      }
    });
    this.dismissModal();

  }

  removeCandidatures(pID, iAdmins) {
    console.log('borrando usuaria')
    this.pService.mRemoveInterestedUser(pID, iAdmins).subscribe(() => {
      console.log('candidatura correctamente eliminada');  
    });  
  }

  calculateMatch(projectID){
    this.mService.mCalculateMatchByProject(projectID).subscribe(() => {
      console.log('Match calculated!');
    });

  }

  get errorCtr() {
    return this.editForm.controls;
  }

  showOptions(x){
    if (x === this.showOption){
      this.showOption = 4
    }
    else (this.showOption = x)
  }

  disableProject(pId: String) {
    this.updateProjectStatus(pId, OfferStatus.INACTIVE);
  }

  enableProject(pId: String) {
  this.updateProjectStatus(pId, OfferStatus.ACTIVE);
  }

  updateProjectStatus(pId: String, projectState: OfferStatus) {
  this.pService.mUpdateProjectState(pId, projectState).subscribe();
  }

  deleteProject(pId){
    this.pService.mDeleteProject(pId)
    .subscribe(() => {
      this.deleted();
      console.log('Project deleted!');
      this.mService.mDeleteProjectMatches(pId).subscribe(() => {
      })
      this.dismissModal();
      this.router.navigate(["project-list"]);
      return;
    });
    
  }

  async deleted(){
    const toast = await this.toastCtrl.create({
      color: 'dark',
      message: 'Proyecto eliminado correctamente!',
      duration: 1500,
      position: 'middle'
    });

    await toast.present();
    this.dismissModal();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }

}
