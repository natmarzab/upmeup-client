import { CommonModule } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TagInputModule } from "ngx-chips";
import { HttpLoaderFactory } from "src/app/app.module";
import { MultilanguageModule } from "src/app/shared/multilanguage/multilanguage.module";
import { ProjectDetailComponentRoutingModule } from "./project-detail-routing.module";
import { ProjectDetailComponent } from "./project-detail.component";
import { ToolbarModule } from "src/app/shared/toolbar/toolbar.module";
import { MenuMobileModule } from "src/app/shared/menu-mobile/menu-mobile.module";


@NgModule({
   imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    TagInputModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ProjectDetailComponentRoutingModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
   ],
   declarations: [ProjectDetailComponent]
})
export class ProjectDetailComponentModule {}