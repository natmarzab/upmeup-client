import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { EditProjectComponent } from '../edit-project/edit-project.component';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { MatchService } from 'src/app/services/match.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {

  projectID: any;
  project: Project;
  userID: any;
  isEnrolled: number = -1;
  enrolledUsers: any[] = [];
  enrolledUsersInfo: any[] = [];
  adminsInfo: any [] = [];
  isAdmin: any;
  match: any;
  allMatches: any[] = [];
  userInfo: User;

  constructor(private aRoute: ActivatedRoute,
              public pService: ProjectService,
              public mController: ModalController,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private translateList: MultilanguageComponent,
              private uService: UserService,
              private router: Router,
              private mService: MatchService) { }

  ngOnInit(): void {
    this.projectID = this.aRoute.snapshot.params.id;
    this.userID = sessionStorage.getItem('userid');
    this.getMatches(this.userID);
    this.qGetProject(this.projectID);
    
  }

  getMatches(userID: string){
    this.mService.qGetMatches(this.userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.allMatches = item.getMatches.filter(match => (match.externalID === this.projectID));
      if (this.allMatches.length !== 0){
        this.match = Math.round(this.allMatches[0].matchScore.total);
      } 
    })
  }

  

  qGetProject(projectId: string){
    this.pService.qGetProject(projectId).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      if (!item) {
        console.log('Ops, sembla que no hi han dades que mostrar....');
      } else {
        
        this.project = {
          ...item.getProject,
          user: {
            ...item.getProject.user
          } 
        };

        this.isEnrolled = this.project.interestedUsers.findIndex(u => u._id === this.userID);
        this.enrolledUsers = this.project.interestedUsers;
        this.enrolledUsersInfo = [];
        if(this.enrolledUsers.length > 0){
          this.getUserInfo();
        }
        if(this.project.admins.length > 0){
          this.getAdminsInfo();
        }
        if(JSON.stringify(this.project.admins).includes(this.userID)){
          this.isAdmin = 'true';
        }
      }
    });
  }

  onDownloadFile(pId: string, file: string){
    this.pService.qGetDownloadFile(pId, file)
      .subscribe(response => {
        window.open(response.data.downloadFile.presignedUrl);
      });
  }

  async editModal() {

      const editModal = await this.mController.create({
        component: EditProjectComponent,
        componentProps: {
          projectData: this.project
        },
        animated: true,
        cssClass: 'modalCss'
      });
      
      await editModal.present();

  }

  addEnrroled(pID, pTitle) {
    this.pService.mAddInterestedUser(pID, this.userID).subscribe(() => {
      this.enrolled();
    });   
  }

  async enrolled() {
   // this.loadingCtrl.dismiss();
    this.translateList.translate.stream('toast-project-added').subscribe(async (value) => {
            const toast = await this.toastCtrl.create({
              color: 'dark',
              message: value,
              duration: 3000,
              position: 'bottom'
            });
            toast.present();
          });  
  }
  removeCandidatures(pID, pTitle) {
    console.log('borrando usuaria')
    this.pService.mRemoveInterestedUser(pID, this.userID).subscribe(() => {
      this.removed();  
    });  
  }
  async removed() {
    // this.loadingCtrl.dismiss();
     this.translateList.translate.stream('toast-project-removed').subscribe(async (value) => {
             const toast = await this.toastCtrl.create({
               color: 'dark',
               message: value,
               duration: 3000,
               position: 'bottom'
             });
             toast.present();
           });  
   }

  getUserInfo(){
    for (let user of this.enrolledUsers){
      this.uService.qGetUser(user._id).valueChanges.pipe(
        map(result => result.data)
      ).subscribe((item) => {
          this.enrolledUsersInfo.push(item.getUser);   
      })
    } 
  }

  getAdminsInfo(){
    this.adminsInfo = [];
    for (let user of this.project.admins){
      this.uService.qGetUser(user._id).valueChanges.pipe(
        map(result => result.data)
      ).subscribe((item) => { 
          this.adminsInfo.push(item.getUser);        
      })
    } 
  }

  goCandidateProfile(userid){
    this.router.navigate(["interested-entity"], {state: {userid: userid }})
  }

  createChat(adminID){
    this.uService.mCreateChatRoomIfNotExists(this.userID, adminID).subscribe(() => {
      this.qGetLoggedUser(this.userID, adminID);
      
    }); 
     
  }

  qGetLoggedUser(userID, adminID){
    this.uService.qGetUser(userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
        this.userInfo = item.getUser;
        this.getChatUrl(adminID);
    })
  }

  getChatUrl(adminID) {
    this.router.navigate([`/chat/${this.userInfo?.rooms?.find(room => room.userId === adminID)?.roomId}`])
  }

}
