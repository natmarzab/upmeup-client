import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { MatchService } from 'src/app/services/match.service';
import { ProjectService } from 'src/app/services/project.service';
import { SectorsService } from 'src/app/services/sectors.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';
import { cityList, statusOptions } from 'src/app/utils/constants';

@Component({
  selector: 'app-all-projects-list',
  templateUrl: './all-projects-list.component.html',
  styleUrls: ['./all-projects-list.component.scss']
})
export class AllProjectsListComponent implements OnInit {

  userInfo: any
  userType: any
  avatar: any
  allProjects: any[] = []
  currPage: number = 0;
  interestProjects: any[];
  userID: any;
  allMatches: any[] = [];
  sharedProjects: boolean;
  ownedProjects: boolean;


  public filters: FormGroup;
  readonly cityList = cityList;
  readonly statusOptions = statusOptions;
  sectorList : any[];
  finalCityList: any[] = [];
  finalSectorList: any[] = [];

  constructor(private uService: UserService,
              private pService: ProjectService,
              public router: Router,
              public fBuilder: FormBuilder,
              private sectorService: SectorsService,
              private translateList: MultilanguageComponent,
              private mService: MatchService) { }

  ngOnInit(){
    this.getLoggedUser();
    this.userID = sessionStorage.getItem('userid');
    this.qGetSectorList();
    this.getMatches(this.userID);
    this.filters = this.fBuilder.group({
      search: [undefined],
      createdDate: [undefined],
      city: [undefined],
      sector: [undefined],
      status: [undefined]
    });
    this.finalCityList = this.translateList.translateCityList();
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type
      this.avatar = item.me.avatarB64;
      // this.candidatures = item.me.candidatures;
    });    
  }

  qGetSectorList() {
    this.sectorService.qGetAllSectors().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sectorList = item.getSectors;
      this.finalSectorList = this.translateList.translateSectorList(this.sectorList);
    });
  }

  getMatches(userID: string){
    this.mService.qGetMatches(userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.allMatches = item.getMatches;
      this.getAllProjects();
    })
  }

  getAllProjects(filters?: any){
    this.pService.qGetAllProjects(filters).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sharedProjects = item.getAllProjects.find(p => (JSON.stringify(p.admins).includes(this.userID) === true && p.user._id !== this.userID)) ? true : false;
      this.ownedProjects = item.getAllProjects.find(p => (JSON.stringify(p.admins).includes(this.userID) === true && p.user._id === this.userID)) ? true : false;
      this.allProjects = item.getAllProjects.filter((p) => (JSON.stringify(p.admins).includes(this.userID) === false && p.state !== 'INACTIVE'))
      .map(project => ({...project, enrolled: JSON.stringify(project.interestedUsers).includes(this.userID)}))  
      for (let match of this.allMatches){
        for (let project of this.allProjects){
          if (project._id === match.externalID){
            project.matchScore = Math.round(match.matchScore.total)
          }
        }
      }
      this.allProjects.sort((a, b) => (a.matchScore > b.matchScore) ? -1 : 1);
    })
    
  }

  toProjectList() {
    this.router.navigate(['/project-list']);
  }

  toInterestProjects() {
    this.router.navigate(['/interest-projects']);
  }

  goProjectDetail(project) {
    const id = project._id;
    this.router.navigate(['/project-detail', id]);
  }

  formatFilters() {
    const filters: any = {}

    if (this.filters.value.search)
      filters.search = this.filters.value.search
    if (this.filters.value.city)
      filters.city = this.filters.value.city
    if (this.filters.value.createdDate) {
      filters.createdDate = new Date();
      filters.createdDate.setHours(0,0,0,0)
      switch(this.filters.value.createdDate) {
        case 'last-24':
          filters.createdDate.setDate(filters.createdDate.getDate() - 1);
          break;
        case 'last-7':
          filters.createdDate.setDate(filters.createdDate.getDate() - 7);
          break;
        case 'last-15':
          filters.createdDate.setDate(filters.createdDate.getDate() - 15);
          break;
      }
    }
    if (this.filters.value.sector)
      filters.sector = this.filters.value.sector
    if (this.filters.value.status)
      filters.status = this.filters.value.status


    if(Object.keys(filters).length==0)
      return undefined;

    return filters 
  }

  onFilter(){
    this.currPage = 0;
    // this.infiniteScroll.disabled = false;
    this.getAllProjects(this.formatFilters());
  }

  clearFilters() {
    this.currPage = 0;
    // this.infiniteScroll.disabled = false;
    this.filters.reset();
    this.getAllProjects();
  }

  redirectToProjectList() {
    this.router.navigate(['/project-list']);
  }

}
