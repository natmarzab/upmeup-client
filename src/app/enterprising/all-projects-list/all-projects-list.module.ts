import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TagInputModule } from 'ngx-chips';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { AllProjectsListComponentRoutingModule } from './all-projects-list-routing.module';
import { AllProjectsListComponent } from './all-projects-list.component';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    FormsModule,
    TagInputModule,
    ReactiveFormsModule,
    IonicModule,
    AllProjectsListComponentRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [AllProjectsListComponent]
})
export class AllProjectsListComponentModule {}