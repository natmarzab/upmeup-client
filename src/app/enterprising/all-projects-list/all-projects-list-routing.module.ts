import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AllProjectsListComponent } from './all-projects-list.component';

const routes: Routes = [
  {
    path: '',
    component: AllProjectsListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllProjectsListComponentRoutingModule {}