import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TagInputModule } from 'ngx-chips';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MenuMobileModule } from 'src/app/shared/menu-mobile/menu-mobile.module';
import { MultilanguageModule } from 'src/app/shared/multilanguage/multilanguage.module';
import { ToolbarModule } from 'src/app/shared/toolbar/toolbar.module';
import { ProjectListComponentRoutingModule } from './project-list-routing.module';
import { ProjectListComponent } from './project-list.component';

@NgModule({
  imports: [
    MultilanguageModule,
    ToolbarModule,
    MenuMobileModule,
    CommonModule,
    FormsModule,
    TagInputModule,
    ReactiveFormsModule,
    IonicModule,
    ProjectListComponentRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
  ],
  declarations: [ProjectListComponent]
})
export class ProjectListComponentModule {}