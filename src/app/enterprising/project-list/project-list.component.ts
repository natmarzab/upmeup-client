
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { map } from 'rxjs';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { CreateProjectComponent } from '../create-project/create-project.component';
import { EditProjectComponent } from '../edit-project/edit-project.component';



@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit{

  userInfo: any
  userType: any
  avatar: any
  userID: string;
  allProjects: any[] = [];
  adminProjects: any[] = [];
  sharedProjects: any[] = [];



  constructor(private uService: UserService,
              private pService: ProjectService,
              public mController: ModalController,
              private router: Router){}

  ngOnInit(): void {
    this.getLoggedUser();
    this.userID = sessionStorage.getItem('userid');
    this.getAllProjects();
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type
      this.avatar = item.me.avatarB64;
      // this.candidatures = item.me.candidatures;
    });    
  }

  getAllProjects(filter?: string){
    this.pService.qGetAllProjects().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.sharedProjects = [item];
      if(filter === 'all' || !filter){
        this.adminProjects = item.getAllProjects
      .filter(project => (JSON.stringify(project.admins).includes(this.userID) === true));
      }
      if(filter === 'owned'){
        this.adminProjects = item.getAllProjects
      .filter(project => (project.user._id === this.userID));
      }
      if(filter === 'shared'){
        this.adminProjects = item.getAllProjects
      .filter(project => (JSON.stringify(project.admins).includes(this.userID) === true && project.user._id !== this.userID));
        this.sharedProjects = this.adminProjects;
      }
      if(filter === 'inactive'){
        this.adminProjects = item.getAllProjects
      .filter(project => (JSON.stringify(project.admins).includes(this.userID) === true && project.state === 'INACTIVE'));
      }
    })
  }

  async createModal() {
    const createModal = await this.mController.create({
      component: CreateProjectComponent,
      animated: true,
      cssClass: 'modalCss'
    });
    await createModal.present();
  }

  goProjectDetail(project) {
    const id = project._id;
    this.router.navigate(['/project-detail', id]);
  }

  async editModal(projectId, event) {
    event.stopPropagation();
    const project = this.adminProjects.find((p) => p._id === projectId)

    if (project) {
      const editModal = await this.mController.create({
        component: EditProjectComponent,
        componentProps: {
          projectData: project
        },
        animated: true,
        cssClass: 'modalCss'
      });
      
      await editModal.present();
    }
  }

}
