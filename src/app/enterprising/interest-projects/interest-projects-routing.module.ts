import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterestProjectsComponent } from './interest-projects.component';

const routes: Routes = [
  {
    path: '',
    component: InterestProjectsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterestProjectsComponentRoutingModule { }