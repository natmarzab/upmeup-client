import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { User } from 'src/app/models/user';
import { MatchService } from 'src/app/services/match.service';
import { ProjectService } from 'src/app/services/project.service';
import { UserService } from 'src/app/services/user.service';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { MultilanguageComponent } from 'src/app/shared/multilanguage/multilanguage.component';


@Component({
  selector: 'app-interest-projects',
  templateUrl: './interest-projects.component.html',
  styleUrls: ['./interest-projects.component.scss']
})
export class InterestProjectsComponent implements OnInit {
  allUsersOffers: any[] = [];
  interestProjectsList: any[] = [];
  enrolledOffers: any[] = [];
  avatarB64: string;
  userType: string;
  avatar: string;
  userInfo: User;
  userID: any;
  allMatches: any[] = [];

  constructor(private pService: ProjectService,
              private uService: UserService,
              public mController: ModalController,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController,
              private translateList: MultilanguageComponent,
              public router: Router,
              private mService: MatchService) { }

  ngOnInit(){
    this.userID = sessionStorage.getItem('userid');
    this.getMatches(this.userID);
    this.getCandidatures();
    this.getLoggedUser();
  }

  getMatches(userID: string){
    this.mService.qGetMatches(userID).valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.allMatches = item.getMatches;
    })
  }

  getCandidatures() {
    this.pService.qGetInterestProjects(this.userID).valueChanges.pipe(
      map(result => result.data.getInterestProjects)
    ).subscribe((interestProjects) => {
      this.interestProjectsList = interestProjects.map(project => ({...project}));
      for (let match of this.allMatches){
        for (let project of this.interestProjectsList){
          if (project._id === match.externalID){
            project.matchScore = Math.round(match.matchScore.total);
          }
        }
      }
    })
    
  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userInfo = item.me;
      this.userType = item.me.type
      this.avatar = item.me.avatarB64;
    });
  }

  // Get offers detail
  goProjectDetail(project) {
    const id = project._id;
    this.router.navigate(['/project-detail', id]);
  }

  removeCandidatures(pID, pTitle) {
    this.pService.mRemoveInterestedUser(pID, this.userID).subscribe(() => {
      this.removed();
      console.log('intento de borrado de usuaria')
    });  
  }
  async removed() {
    // this.loadingCtrl.dismiss();
     this.translateList.translate.stream('toast-project-added').subscribe(async (value) => {
             const toast = await this.toastCtrl.create({
               color: 'dark',
               message: value,
               duration: 3000,
               position: 'bottom'
             });
             toast.present();
           });  
   }


}
