import { Component, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/services/user.service';
import { cityList, languageList } from 'src/app/utils/constants';

@Component({
  selector: 'app-multilanguage',
  templateUrl: './multilanguage.component.html',
  styleUrls: ['./multilanguage.component.css']
})
export class MultilanguageComponent {
  languageListTranslated: any[] = [];
  finalLangList: any[] = [];
  cityListTranslated: any[] = [];
  finalCityList: any[] = [];
  sectorListTranslated: any[] = [];
  skillsListTranslated: any[] = [];

  @Input() color?: string = 'white'
  @Input() position?: string = 'absolute'
  
  constructor(
    private uService: UserService,
    public translate: TranslateService) {
      translate.use(window.localStorage.getItem('language')? window.localStorage.getItem('language'): 'es');
    }


  //Multilanguage
  useLanguage(language: string): void {
    window.localStorage.setItem('language', language);
    this.translate.use(language);
    if (window.sessionStorage.getItem('userid')){
      this.uService.mUserPreferencesUpdate(window.sessionStorage.getItem('userid'), {language}).subscribe(() => {
        console.log('language stored!')
      })
    }    
  }

  //Translate and sort lists alphabetically
  translateCityList(){  

      if (this.cityListTranslated){
        this.cityListTranslated = []
      } 
 
      for (let city of cityList){
        this.translate.get(city).subscribe((cityTranslated: string) => {
            this.cityListTranslated.push({value:city, label:cityTranslated})
        }) 
      }
      return this.alphabeticalOrder(this.cityListTranslated)

  }   

  translateLangList(){
      if (this.languageListTranslated){
        this.languageListTranslated = []
      } 

      for (let language of languageList){
        this.translate.get(language).subscribe((languageTranslated: string) => {
            this.languageListTranslated.push({value:language, label:languageTranslated})
        }) 
      }

      return this.alphabeticalOrder(this.languageListTranslated)
  }

  translateSectorList(sectorsList){
    if (this.sectorListTranslated){
      this.sectorListTranslated = []
    } 

    for (let sector of sectorsList){
      let sectorTranslated = this.translate.instant(sector.name)
      this.sectorListTranslated.push({value:sector, label:sectorTranslated})
    }

    return this.alphabeticalOrder(this.sectorListTranslated)
  }

  translateSkillsLists(skillsList){  
    if (this.skillsListTranslated){
      this.skillsListTranslated = []
    } 

    for (let skill of skillsList){
      let skillsTranslated = this.translate.instant(skill.name)
      this.skillsListTranslated.push({label: skillsTranslated, name: skill.name, _id: skill._id})
    }
    return this.alphabeticalOrder(this.skillsListTranslated)
  }   


  alphabeticalOrder(translatedList){   
    return translatedList.sort((a, b) => new Intl.Collator(this.translate.currentLang).compare(a.label, b.label));
  }

} 



