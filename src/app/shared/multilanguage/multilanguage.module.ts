import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageComponent } from './multilanguage.component';



@NgModule({
  declarations: [MultilanguageComponent],
  imports: [
  //ngx-translate and loader module
  HttpClientModule,
  TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  exports:[MultilanguageComponent] 

  
})
export class MultilanguageModule { }
