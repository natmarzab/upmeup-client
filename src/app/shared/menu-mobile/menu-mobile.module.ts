import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MenuMobileComponent } from './menu-mobile.component';
import { IonicModule } from '@ionic/angular';
import { MultilanguageModule } from '../multilanguage/multilanguage.module';
import { MultilanguageComponent } from '../multilanguage/multilanguage.component';



@NgModule({
  declarations: [MenuMobileComponent],
  imports: [
  CommonModule,
  RouterModule,
  IonicModule,
  MultilanguageModule,
  //ngx-translate and loader module
  HttpClientModule,
  TranslateModule.forChild({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  exports:[MenuMobileComponent],
  providers:[MultilanguageComponent]

  
})
export class MenuMobileModule { }