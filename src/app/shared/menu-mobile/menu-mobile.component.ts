import { Component, TemplateRef } from "@angular/core";
import { MenuController, ModalController } from "@ionic/angular";
import { map } from "rxjs";
import { CreateOfferPage } from "src/app/pages/create-offer/create-offer.page";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "src/app/services/user.service";
import { MultilanguageComponent } from "../multilanguage/multilanguage.component";
import { MultilanguageModule } from "../multilanguage/multilanguage.module";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";

@Component({
    selector: 'app-menu-mobile',
    templateUrl: './menu-mobile.component.html',
    styleUrls: ['./menu-mobile.component.scss']
  })
  export class MenuMobileComponent {
    userType: string;
    modalRef: BsModalRef;

    constructor(
        public auth: AuthService,
        public menuCtrl: MenuController,
        private mController: ModalController,
        public multilengague: MultilanguageComponent,
        private uService: UserService,
        private modalService: BsModalService) {
          this.auth.isLoggedIn.subscribe(isLoggedIn => {
            if(isLoggedIn)
              this.qGetLoggedUser();
          })       
      };
    
      qGetLoggedUser() {    
        this.uService.qGetMe().valueChanges.pipe(
          map(result => result.data)
        ).subscribe((item) => {
          this.userType = item.me.type;    
        });    
      }

          /**
     * Modal to Create New Offer
     */
    async createModal() {
      const createModal = await this.mController.create({
        component: CreateOfferPage,
        animated: true,
        cssClass: 'modalCss'
      });
      await createModal.present();
    }

    async openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
    }

  }