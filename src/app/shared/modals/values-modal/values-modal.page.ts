/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable max-len */
import { Component, EventEmitter, HostListener, OnDestroy, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { map } from 'rxjs/operators';
import { SoftSkill } from 'src/app/models/soft-skill.model';
import { SoftskillsService } from 'src/app/services/softskills.service';
import { UserService } from 'src/app/services/user.service';
import { MultilanguageComponent } from '../../multilanguage/multilanguage.component';

@Component({
  selector: 'app-values-modal',
  templateUrl: './values-modal.page.html',
  styleUrls: ['./values-modal.page.scss'],
})
export class ValuesModalPage implements OnInit, OnDestroy {
  skillsList: any[] = [];
  maxElementCheckbox = 6;
  skillsSelected: any[] = [];
  currentModal = null;
  userID = sessionStorage.getItem('userid');
  skills: any[] = [];
  userSkills: any[] = [];
  selected = 0;
  selectedSkills: Set<String> = new Set();
  finalSkillsList: {name: any , output: any, _id: any, label: any} [] = [{"name": "", "_id": "", "output": "", "label":""}];
  skillsListTranslated: {name: any , output: any, _id: any} [] = [];

  @Output()
  sendSelectedSkills: EventEmitter<any> = new EventEmitter<any>();

  constructor(private modalController: ModalController,
              private softSkillService: SoftskillsService,
              private uService: UserService,
              private translate: TranslateService, 
              private translateList: MultilanguageComponent) {}

  ngOnInit() {
    this.qsoftSkillsQuery();
    this.getLoggedUser();
    // this.userSkills = JSON.parse(sessionStorage.getItem('uSelectedSkills'));
    
  }

  /**
   * Get softSkills from DB.
   */
  qsoftSkillsQuery() {
    this.softSkillService.qGetAllSkills().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.skillsList = item.getSoftskills;
      this.finalSkillsList = this.translateList.translateSkillsLists(this.skillsList)
    });

  }

  getLoggedUser() {
    this.uService.qGetMe().valueChanges.pipe(
      map(result => result.data)
    ).subscribe((item) => {
      this.userSkills = item.me.softSkills;
      this.selectedSkills = new Set(this.userSkills.map(s => s._id));
    this.skillsSelected = this.userSkills;
      
    });
  }
 
  saveSkills() {
    this.useSessionStorage(this.skillsSelected);
    this.skillsSelected.forEach(element => {
      this.sendSelectedSkills.emit(element);
      this.skills.push({"_id": element._id, "name": element.name});
    });
    this.mEditUserSkills(this.skills);
  }

  /**
   * Save new values for "Skills"
   * @param skills
   */
  mEditUserSkills(skillsList: SoftSkill[]) {
    if(this.userID) {
      this.uService.mUserSkillUpdate(this.userID, skillsList)
      .subscribe((response) => {
        console.log('Edition Done!');
      });
    }

    this.dismissEditModal(this.skillsSelected);
  }

  /**
  * Close modal when update
  */
  async dismissEditModal(data) {
    this.modalController.dismiss(data);
  }

  /**
   * Selection of skills
   * @param $event 
   * @param item 
   */
  changeSelection(event, item) {
      if (event.target.checked) {
        this.skillsSelected.push(item);
        this.selectedSkills.add(item._id)
      } else {
        this.skillsSelected.splice(this.skillsSelected.indexOf(item), 1);
        this.selectedSkills.delete(item._id)
      }
  }

  disableCheckbox(id): boolean {
    return (
      this.selectedSkills.size >= this.maxElementCheckbox &&
      !this.selectedSkills.has(id)
    );
  }

  isChecked(id): boolean {
    return this.selectedSkills.has(id)
  }

  /**
   * Use LocalStogare for save an array of selected skills
   * @param uSkills 
   */
  useSessionStorage(uSkills) {
    sessionStorage.setItem('uSelectedSkills', JSON.stringify(uSkills));
  }

  /** Modal functions if "button back" is clicked */
  ngOnDestroy() {
    if (window.history.state.modal) {
      history.back();
    }
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.modalController.dismiss();
  }

}

