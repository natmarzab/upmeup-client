import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AvatarModalPageRoutingModule } from './avatar-modal-routing.module';
import { AvatarModalPage } from './avatar-modal.page';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from '../../multilanguage/multilanguage.module';
import { BrowserModule } from '@angular/platform-browser';
import { ImageCropperModule} from 'ngx-image-cropper'

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    IonicModule,
    AvatarModalPageRoutingModule,
    ImageCropperModule,
    //ngx-translate and loader module
    HttpClientModule,   
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })

  ],
  declarations: [AvatarModalPage]
})
export class AvatarModalPageModule {}
