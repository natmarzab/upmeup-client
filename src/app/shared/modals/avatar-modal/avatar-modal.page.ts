import { Component, HostListener, OnDestroy, OnInit, Output } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";
import { LoadingController, ModalController, ToastController } from "@ionic/angular";
import { ImageCroppedEvent, LoadedImage } from "ngx-image-cropper";
import { map } from "rxjs";
import { UserService } from "src/app/services/user.service";
import { MultilanguageComponent } from "../../multilanguage/multilanguage.component";


@Component({
    selector: 'app-avatar-modal',
    templateUrl: './avatar-modal.page.html',
    styleUrls: ['./avatar-modal.page.scss'],
  })

  export class AvatarModalPage implements OnInit {

    file: any = [];
    avatarBase64: string;
    userID = sessionStorage.getItem('userid');
    avatar: string;
    resizedBase64: any;
    avatarFile: any;
    imageChangedEvent: any = '';
    croppedImage: any = '';
    uploaded: string;
    
    

    constructor(private sanitizer: DomSanitizer,
                private uService: UserService,
                private mdlController: ModalController,
                private toastCtrl: ToastController,
                public loadingCtrl: LoadingController,
                private translateList: MultilanguageComponent,
      ){}

      ngOnInit() {
        this.qGetAvatar(this.userID);
      }

    onSelectNewAvatar(event): any {
        this.uploaded = "";
        this.imageChangedEvent = event;  
        
    }

    imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = this.sanitizer.bypassSecurityTrustUrl(event.objectUrl);
      this.convertBase64(event.blob).then((imagen: any) => {
        this.avatarBase64 = imagen.base;
      })
    }
    

    convertBase64 = async ($event: any) => new Promise ((resolve, reject) => {
      try {
        const unsafeImg = window.URL.createObjectURL($event);
        const image = this.sanitizer.bypassSecurityTrustUrl(unsafeImg);
        const reader = new FileReader();
        reader.readAsDataURL($event);
        reader.onload = () => {
          resolve({
            base: reader.result
          });
        };
        reader.onerror = error => {
          resolve({
            base: null
          })
        }
      } catch(e){
        return null;
      }
    })

    uploadImage(userID: any, avatar: string){
      sessionStorage.setItem("avatarB64", avatar);
      this.uService.mUserAvatarUpdate(userID, avatar)
      .subscribe((response) => {
        console.log('Avatar edited!');
        this.success();
      })
      this.avatarBase64 = "";
      this.uploaded = 'true'
    
    }

    // Alert to confirm an action
  async success() {
    this.loadingCtrl.dismiss();
    this.translateList.translate.stream('toast-updated-avatar').subscribe(async (value) => {
      const toast = await this.toastCtrl.create({
        color: 'dark',
        message: value,
        duration: 3000,
        position: 'middle'
      });
      toast.present();
    });  
  }

    qGetAvatar(userId: any){
      this.uService.qGetMe().valueChanges.pipe(
        map(result => result.data)
      ).subscribe((item) => {
        if (!item){
          console.log("Aun no hay avatar seleccionado!")
        } 
        else {
        this.avatar = item.me.avatarB64;
        }
      });
      
    }


  
    redirectUploadFile(){
      const redirect = document.getElementById("upload-avatar");
      console.log(redirect)
      redirect.click();
    }

  

      /**
      * Close modal when update
      */
      async dismissEditModal() {
        this.mdlController.dismiss();
      }

      @HostListener('window:popstate', ['$event'])
      dismissModal() {
        this.mdlController.dismiss();
      }
      }

  
  