import { Component, HostListener, Input } from "@angular/core";
import { LoadingController, ModalController, ToastController } from "@ionic/angular";
import { UserService } from "src/app/services/user.service";
import { MultilanguageComponent } from "../../multilanguage/multilanguage.component";


@Component({
  selector: 'app-file-modal',
  templateUrl: './file-modal.page.html',
  styleUrls: ['./file-modal.page.scss'],
})

export class FileModalPage { 

  file: any;
  LoggedUser: any;
  selected: any;

  @Input() mode: String;

  constructor(private uService: UserService,
    private mdlController: ModalController,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private translateList: MultilanguageComponent,

  ) {
    this.uService.qGetMe().valueChanges.subscribe((response) => {
      this.LoggedUser = response.data.me;
    })
  }


  onSelectFile(event): any {
    this.file = event.target.files[0];
  }



  async uploadFile() {
    if (this.file.name.split('.').pop() === 'pdf') {
      const filename = `${this.mode}_${this.LoggedUser.name.split(' ').join('_')}_${this.LoggedUser.surname.split(' ').join('_')}.pdf`
      this.uService.qGetUploadUrl(filename)
        .subscribe(response => {
          const formData = new FormData();
          formData.append('file', this.file)
          fetch(response.data.uploadUrl.presignedUrl, { method: 'PUT', body: formData }).then((response) => {
            if (response.ok) {
              if (this.mode === 'CV')
                this.uService.mUpdateCV(filename).subscribe(() => {
                  this.success();
                  this.dismissEditModal();
                })
              else if (this.mode === 'Cover_Letter')
                this.uService.mUpdateCoverLetter(filename).subscribe(() => {
                  this.dismissEditModal();
                })
            }
          });
        });
    } else {
      this.loadingCtrl.dismiss();
      this.translateList.translate.stream('toast-files-should-be-pdf').subscribe(async (value) => {
        const toast = await this.toastCtrl.create({
          color: 'dark',
          message: value,
          duration: 3000,
          position: 'middle'
        });
        toast.present();
      });  
      return;
    }
  }

     // Alert to confirm an action
     async success() {
      const toast = await this.toastCtrl.create({
        color: 'dark',
        message: '¡Archivo actualizado correctamente!',
        duration: 3000,
        position: 'middle',
        
      });
  
      await toast.present();
    }

  /**
  * Close modal when update
  */
  async dismissEditModal() {
    this.mdlController.dismiss();
  }

  @HostListener('window:popstate', ['$event'])
  dismissModal() {
    this.mdlController.dismiss();
  }
}


