import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpLoaderFactory } from 'src/app/app.module';
import { MultilanguageModule } from '../../multilanguage/multilanguage.module';
import { FileModalPageRoutingModule } from './file-modal-routing.module';
import { FileModalPage } from './file-modal.page';

@NgModule({
  imports: [
    MultilanguageModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    IonicModule,
    FileModalPageRoutingModule,
    //ngx-translate and loader module
    HttpClientModule,   
    TranslateModule.forChild({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })

  ],
  declarations: [FileModalPage]
})
export class FileModalPageModule {}
