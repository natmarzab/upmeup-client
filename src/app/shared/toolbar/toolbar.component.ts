import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { MenuController } from "@ionic/angular";
import { AuthService } from "src/app/services/auth.service";

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
  })
  export class ToolbarComponent {
    constructor(
        public auth: AuthService,
        private router: Router,
        public menuCtrl: MenuController
    ){}

    logOut() {
        this.auth.onLogout();
        this.menuCtrl.enable(false);
        this.router.navigate(['/login']);
      }
  }