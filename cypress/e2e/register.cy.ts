describe('Registration', () => {
  it('Should be able to log in', () => {
    
    const slug = 'cypress-user'
    const w_interval = 600
    
    cy.viewport(780, 1080)
    
    let u = {   
        type: 'Tr', // Trabajador / Treball
        name: `${slug}`,
        surname: 'migueláñez',
        city: 'Burgos',
        mail: `${slug}@librecoop.es`,
        pass: 'Holaquetal11234',
        jobPos: 'Junior automation tester',
        sector: 'Admin',
        exp: '5',
        // lastJob: 'Spent so many hours just trying to create an account in this silly platform that became cy-proficient',
        lastJob: '20 characters is enough.',
        edu: 'Master',
        lang: 'Árab',
        skills: ['Robótica', 'Cypress']
        
    }
    
            
    // Login
    cy.visit('/register')
    cy.wait(w_interval)
    // cy.contains('Comen').click() // click "Començar"
    cy.contains(u.type).click() // Empresa/treballadora
      
    cy.wait(w_interval)
    cy.get('input[id=iName]').clear().type(u.name) 
    cy.get('input[id=iSurname]').clear().type(u.surname) 
    
    cy.get('ion-select[id=iCity]').click()
    cy.wait(w_interval)
    cy.contains('.action-sheet-button', u.city).click();
    // cy.contains('.action-sheet-button', 'Burgos').click();
    
    cy.contains('Continu').click(); // Buton "Continuar"
    cy.wait(w_interval)
    
    // cy.get('#iEmail').scrollIntoView().should('be.visible').type(u.mail) 
    cy.get('#iEmail').click().type(u.mail) 
    cy.get('input[id=iPassword]').clear().type(u.pass) 
    cy.get('input[id=confirmPassword]').clear().type(u.pass) 
    cy.get('[cy-data="continueSlide2"]').click()
    cy.wait(w_interval)
    
    cy.get('input[id=iJobPos]').clear().type(u.jobPos) 
    cy.get('ion-select#iSector').click()
    // cy.contains('.action-sheet-button', u.sector).click();
    cy.contains('.alert-checkbox-button', u.sector).click();
    cy.contains('button.alert-button', 'OK').click(); // OK
    
    cy.get('#iExp').click().type(u.exp) 
    cy.get('[cy-data="continueSlide3"]').click()
    cy.wait(w_interval)
    
    cy.get('#iLastJob').type(u.lastJob) 
    cy.get('[cy-data="continueSlide4"]').click()
    cy.wait(w_interval)
    
    // Skills
    cy.get('ion-toggle').eq(3).click() 
    cy.get('ion-toggle').eq(6).click().wait(100)
    cy.get('ion-toggle').eq(8).click().wait(100) 
    cy.get('ion-toggle').eq(11).click() .wait(100)
    cy.get('ion-toggle').eq(13).click() .wait(100)
    cy.get('ion-toggle').eq(15).click() .wait(100)
    cy.get('[cy-data="saveSkills"]').click()
    cy.wait(w_interval)
    
    // Estudios
    cy.get('input[id=iEduc]').clear().type(u.edu) 

    cy.get('#iLang').click()   
    cy.contains('.alert-checkbox-button', u.lang).click()
    cy.get('.alert-button').eq(1).click();

    cy.get('[cy-data="competences"]').click()
    .type(u.skills[0]).trigger('submit')
    cy.get('form.ng-dirty').eq(0).submit()

    // .type(u.skills[1]).type('\n')


    
    
    // Languages
    // Árab
    // OK
    
    
    // cy.get('[cy-data="continueSkills"]').click()
    
    
    // Privacy check
    
    
    
  })
})