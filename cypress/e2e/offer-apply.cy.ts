describe('Basic offer application', () => {
  it('Candidate should be able to apply to an existing offer', () => {
    
    let ent_login = 'arse@librecoop.es'
    let password = 'Passw0rd'
    const w_interval = 500
    
    const o = {
        title : 'Super txatxi full stack trabajador',
        level : 'FPGA',
        skills : ['Liderazgo','Compromiso','Madurez','Buen humor'],
        salary : '43.000',
        description : 'El puestako de tu vida si esto no fuera un cypress :D',
        requirements : 'Hacer de todo un poco y un poco de tó',
        more_req:', y si acaso alguna cosilla más...',
        
    }
            
    // Login
    cy.visit('/login')
    cy.get('input[id=userEmail]')
      .clear()
      .type(ent_login) 
    
    cy.get('input[id=userPassword]')
      .clear()
      .type(password) 
    // Click Enter button
    
    cy.get('ion-button[type=submit]').click()
    cy.wait(w_interval)
    
    // Close Ads
    cy.get('ion-row.md > .ion-color').click()
    cy.wait(w_interval)
    
    // Ofertas - bottom menu
    cy.get('ion-icon.ofertas-ico')
        .should('be.visible').click() 
    cy.wait(2 * w_interval)
    // cy.wait(w_interval) // TODO - Wait to be visible
    
    // cy.get('.ion-icon .ofertas-ico'), 'Ofertas').click(); // Ofertas
    // cy.contains('ion-col > .ofertas-ico', 'Ofertas').click(); // Ofertas
    
    // Go to known offer
    cy.visit('/offer-detail/655ba09bd1a057c51f468db0')
    
    // Scroll down and Click on "inscribirme"
    cy.get('[cy-data="sign-me-up"]')
        .should('be.visible').click() // 1st option.click() // Finalitzar
    cy.wait(w_interval) // TODO - Wait to be visible
    
    
    
    
  })
})