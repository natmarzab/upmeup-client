describe('Registration', () => {
  it('Should be able to log in', () => {
    
    const slug = 'mojopicon'
    const w_interval = 300
    
    let e = {   
        type: 'Empresa',
        name: `${slug}Coop`,
        legal: 'Coop',
        website: `${slug}.net`,
        city: 'Ceuta',
        mail: `info@${slug}.coop`,
        pass: 'Passw0rd',
        jobPos: 'Junior automation tester',
        sector: 'Aliment',
        // exp: '5', // Antiguitat // -clicked 3 times
        // lastJob: 'Spent so many hours just trying to create an account in this silly platform that became cy-proficient',
        lastJob: '20 characters is enough.',
        lang: 'Árab',
        
    }
    
            
    // Login
    cy.visit('/register')
    cy.wait(w_interval)
    // cy.contains('Comen').click() // click "Començar"
    cy.contains(e.type).click() // Empresa/treballadora
    cy.wait(w_interval)
      
    cy.get('input[id=iName]').clear().type(e.name) 
    
    // Forma jurid
    cy.get('ion-select[id=iLegal]').click()
    cy.wait(w_interval)
    cy.contains('.action-sheet-button', e.legal).click();
    
    cy.get('ion-select[id=iCity]').click()
    cy.contains('.action-sheet-button', e.city).click();
    cy.contains('Continu').click(); // Buton "Continuar"
    cy.wait(w_interval)
    
    // cy.get('#iEmail').scrollIntoView().should('be.visible').type(e.mail) 
    cy.get('input[id=iEmail]').click().type(e.mail) 
    cy.get('input[id=iPassword]').clear().type(e.pass) 
    cy.get('input[id=confirmPassword]').clear().type(e.pass) 
    cy.get('[cy-data="continueSlide2"]').click()
    cy.wait(w_interval)
    
    // Website
    // cy.get('input[id=iWebsite]').clear().type(e.website) 
    
    // Sector / antiguitat
    cy.get('ion-select#iSector').click()
    cy.wait(w_interval)
    cy.contains('.alert-checkbox-button', e.sector).click();
    cy.contains('button.alert-button', 'OK').click(); // OK
    cy.wait(w_interval)
    
    // + + + 
    // cy.get('#iExp').click().type(e.exp) 
    cy.get('[cy-data="plus-one"]').click().click().click() // #iExp
    cy.get('[cy-data="continueSlide3"]').click()
    cy.wait(w_interval)
    
    // Qué feu
    cy.get('#iLastJob').type(e.lastJob) 
    cy.get('[cy-data="continueSlide4"]').click()
    cy.wait(w_interval)
    
    // Skills
    // cy.contains('Afegir').click()
    cy.get('ion-toggle').eq(3).click() // 4, 6 , 7
    cy.get('ion-toggle').eq(6).click() 
    cy.get('ion-toggle').eq(8).click() 
    cy.get('ion-toggle').eq(10).click() 
    
    cy.get('[cy-data="finish-ent"]').click() // Finalitzar
    
    // Languages
    // Árab
    // OK
    
    
  })
})